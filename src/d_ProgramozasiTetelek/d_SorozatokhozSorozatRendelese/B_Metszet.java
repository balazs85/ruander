package d_ProgramozasiTetelek.d_SorozatokhozSorozatRendelese;

public class B_Metszet {
    public static void main(String[] args){
        int[] szamok1 = {19, 17, 22, 43, 86};
        int[] szamok2 = {54, 86, 11, 19, 18, 22};
        int[] eredmeny = metszet(szamok1, szamok2);
    }

    private static int[] metszet(int[] x, int[] y) {
        int[] eredmeny = new int[x.length];
        int db = 0;
        for (int i = 0; i < y.length; i++) {
            if (eleme_e(y[i], x)){
                eredmeny[db] = y[i];
                db++;
            }
        }
        return java.util.Arrays.copyOf(eredmeny, db);
    }

    private static boolean eleme_e(int elem, int[] x) {
        int i = 0;
        while(i<x.length && x[i]!=elem){
            i++;
        }
        return i<x.length;
    }
}
