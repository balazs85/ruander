package d_ProgramozasiTetelek.c_SorozathozSorozatRendelese;

public class B_Kivalogatas {
public static void main(String[] args){
    double[] hom = {19, 17, 19, 20, 19, 22, 20};
    int[] hom2 = egyHijanHusz(hom);
    for (int i = 0; i < hom2.length; i++) {
        System.out.println(hom2[i]);
    }
}

private static int[] egyHijanHusz(double[] hom) {
    int[] eredmeny = new int[hom.length];
    int db = 0;
    for (int i = 0; i < hom.length; i++) {
        if (hom[i] == 19){
            eredmeny[db] = i;
            db++;
        }
    }
    return java.util.Arrays.copyOf(eredmeny, db);
}
}
