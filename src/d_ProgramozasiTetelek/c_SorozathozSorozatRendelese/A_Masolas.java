package d_ProgramozasiTetelek.c_SorozathozSorozatRendelese;

public class A_Masolas {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        double[] hom2 = fahrenheitben(hom);
    }

    private static double[] fahrenheitben(double[] hom) {
        double[] eredmeny = new double[hom.length];
        for (int i = 0; i < hom.length; i++) {
            eredmeny[i] = (double)9 / 5 * hom[i] + 32;
        }
        return eredmeny;
    }
}
