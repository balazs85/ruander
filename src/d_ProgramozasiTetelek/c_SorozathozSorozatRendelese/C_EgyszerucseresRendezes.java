package d_ProgramozasiTetelek.c_SorozathozSorozatRendelese;

public class C_EgyszerucseresRendezes {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        double[] hom2 = rendez(hom);
    }

    private static double[] rendez(double[] x) {
        for (int i = 0; i < x.length - 1; i++) {
            for (int j = i + 1; j < x.length; j++) {
                if (x[i] > x[j]){
                    double c = x[i];
                    x[i] = x[j];
                    x[j] = c;
                }
            }
        }
        return x;
    }
}
