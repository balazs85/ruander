package d_ProgramozasiTetelek.a_Fuggveny;

public class A_Osszeadas {
    public static void main(String[] args){
        int a = 2;
        int b = 3;
        int c = osszeg(a, b);
        System.out.println(c);
    }

    private static int osszeg(int a, int b) {
        return a + b;
    }
}
