package d_ProgramozasiTetelek.a_Fuggveny;

public class B_Kiiras {
    public static void main(String[] args){
        String[] munkanapok = {"Hétfő", "Kedd", 
            "Szerda", "Csütörtök", "Péntek"};
        kiir(munkanapok);
    }

    private static void kiir(String[] munkanapok) {
        for (int i = 0; i < munkanapok.length; i++) {
            System.out.println(munkanapok[i]);
        }
    }
}
