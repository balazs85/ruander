package d_ProgramozasiTetelek.a_Fuggveny;

public class C_Osszeadas {
    public static void main(String[] args){
        int a = 2; int b = 3;
        int[] szamok = {1, 6, 3, 9, 33};
        System.out.println(osszeg(a, b));
        System.out.println(osszeg(szamok));        
    }

    private static int osszeg(int a, int b) {
        return a + b;
    }

    private static int osszeg(int[] t) {
        int eredmeny = 0;
        for (int i = 0; i < t.length; i++) {
            eredmeny = eredmeny + t[i];
        }
        return eredmeny;
    }
}
