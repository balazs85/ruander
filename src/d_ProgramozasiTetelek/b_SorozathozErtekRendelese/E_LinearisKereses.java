package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class E_LinearisKereses {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        int valasz = tizAlatt(hom);
        if (valasz == -1){
            System.out.println("Nem volt ilyen");
        } else {
            System.out.println(tizAlatt(hom));
        }
    }

    private static int tizAlatt(double[] x) {
        int i = 0;
        while (i < x.length && x[i]>=10){
            i++;
        }
        if (i < x.length){ 
            return i;
        } else { 
            return -1;
        }
    }
}
