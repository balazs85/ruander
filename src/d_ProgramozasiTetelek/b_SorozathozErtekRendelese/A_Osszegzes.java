package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class A_Osszegzes {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(atlag(hom));
    }

    private static double osszeg(double[] t) {
        double eredmeny = 0;
        for (int i = 0; i < t.length; i++) {
            eredmeny = eredmeny + t[i];
        }
        return eredmeny;
    }

    private static double atlag(double[] hom) {
        return osszeg(hom)/hom.length;
    }
}
