package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class D_Kivalasztas {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(nagyobbMintHusz(hom));
    }

    private static int nagyobbMintHusz(double[] x) {
        int i = 0;
        while (x[i]<=20){
            i++;
        }
        return i;
    }
}
