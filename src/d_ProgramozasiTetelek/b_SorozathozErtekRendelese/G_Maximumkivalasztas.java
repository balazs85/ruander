package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class G_Maximumkivalasztas {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(legnagyobb(hom));
    }

    private static int legnagyobb(double[] x) {
        int max = 0;
        for (int i = 1; i < x.length; i++) {
            if (x[i] > x[max]) {
                max = i;
            }
        }
        return max;
    }
}
