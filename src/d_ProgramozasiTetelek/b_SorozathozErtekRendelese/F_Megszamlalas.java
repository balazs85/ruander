package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class F_Megszamlalas {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(hanyszorTizenkilenc(hom));
    }

    private static int hanyszorTizenkilenc(double[] x) {
        int db = 0;
        for (int i = 0; i < x.length; i++) {
            if (x[i] == 19) {
                db++;
            }
        }
        return db;
    }
}
