package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class I_Fuggvennyel {
    public static void main(String[] args){
        int[] szamok = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(primekSzama(szamok));
    }

    private static int primekSzama(int[] x) {
        int db = 0;
        for (int i = 0; i < x.length; i++) {
            if (prim_e(x[i])){
                db++;
            }
        }
        return db;
    }

    private static boolean prim_e(int szam) {
        int i = 2;
        while(i < Math.sqrt(szam) && szam % i != 0){
            i++;
        }
        return i >= Math.sqrt(szam);
    }
}
