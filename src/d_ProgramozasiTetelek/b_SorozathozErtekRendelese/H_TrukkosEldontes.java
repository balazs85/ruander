package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class H_TrukkosEldontes {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(emelkedo_e(hom));
    }

    private static boolean emelkedo_e(double[] x) {
        int i = 0;
        while (i < x.length - 1 && x[i] <= x[i+1]){
            i++;
        }
        return i == x.length -1;
    }
}
