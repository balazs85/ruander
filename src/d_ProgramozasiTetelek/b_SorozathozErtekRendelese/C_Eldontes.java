package d_ProgramozasiTetelek.b_SorozathozErtekRendelese;

public class C_Eldontes {
    public static void main(String[] args){
        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println(mindNagyobb_e(hom));
    }

    private static boolean mindNagyobb_e(double[] x) {
        int i = 0;
        while (i < x.length && x[i]>=20){
            i++;
        }
        return i == x.length;
    }
}
