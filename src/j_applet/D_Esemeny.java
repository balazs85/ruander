/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j_applet;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JApplet;

/**
 *
 * @author nbalazs
 */
public class D_Esemeny extends JApplet implements MouseListener {

    ArrayList<String> esemenyek;

    public void init() {
	addMouseListener(this);
        esemenyek = new ArrayList();
        esemenyTortent("applet inicalizálás");
    }

    public void start() {
        esemenyTortent("applet elindítása ");
    }

    public void stop() {
        esemenyTortent("applet megállítása ");
    }

    public void destroy() {
        esemenyTortent("applet törlése");
    }

    void esemenyTortent(String e) {
        esemenyek.add(e);
        System.out.println(e);        
        repaint();
    }

    public void paint(Graphics g) {
        for (int i = 0; i < esemenyek.size(); i++) {
            g.drawString(esemenyek.get(i), 10, 20+i*15);
        }    
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        esemenyTortent("egérkattintás (" + e.getX()+", "+e.getY()+")"); 
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        esemenyTortent("egérmegnyomás (" + e.getX()+", "+e.getY()+")");    
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        esemenyTortent("egérelengedés (" + e.getX()+", "+e.getY()+")");    
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        esemenyTortent("egér a rajzterületre ér (" + e.getX()+", "+e.getY()+")");    
    }

    @Override
    public void mouseExited(MouseEvent e) {
        esemenyTortent("egér a rajzterületről kilép (" + e.getX()+", "+e.getY()+")");            
    }
}
