package j_applet;

import j_applet.H_SajatKep;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JApplet;

public class H_KepVonszolas extends JApplet implements MouseListener, MouseMotionListener{
    //TODO: talán lehetne még visszafejleszteni
    H_SajatKep csillag;
    H_SajatKep kivalasztott;
    Point holKlikkelt;
    
    public void init() {
        csillag = new H_SajatKep(new Point(100,100), "res/csillag.png");
        addMouseListener(this);
        addMouseMotionListener(this);
    }
    
    
    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        csillag.kirajzol(g);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (csillag.talalt(e.getPoint())){
            kivalasztott = csillag;
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        kivalasztott = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (kivalasztott != null){
            holKlikkelt = e.getPoint();
            kivalasztott.kirajzol(this.getGraphics(), holKlikkelt);
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    
    

}
