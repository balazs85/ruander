package j_applet;

import java.awt.Graphics;
import javax.swing.JApplet;

public class B_Rajz extends JApplet {

    public void init() {

    }

    public void paint(Graphics g) {
        g.drawRect(10, 10, 50, 80);
        g.drawLine(100, 5, 120, 50);
        g.drawOval(10, 100, 40, 80);
        g.drawString("Hello Világ", 100, 190);
    }
}
