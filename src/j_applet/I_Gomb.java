package j_applet;

import java.awt.Button;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JApplet;

public class I_Gomb extends JApplet implements ActionListener {

    Button kivalasztott;
    
    public void init() {
        setLayout(null);
        
        Button teglaGomb = new Button("Teglalap");
        teglaGomb.setBounds(10, 40, 70, 20);
        teglaGomb.addActionListener(this);
        add(teglaGomb);
        
        Button ellipszisGomb = new Button("Ellipszis");
        ellipszisGomb.setBounds(10, 70, 70, 20);
        ellipszisGomb.addActionListener(this);
        add(ellipszisGomb);     
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth()-1, getHeight()-1);
        if (kivalasztott != null){
            g.drawRect(10, 10, 70, 20);
            g.drawString(kivalasztott.getLabel(), 14, 26);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        kivalasztott = (Button)e.getSource();
        repaint();
    }
}