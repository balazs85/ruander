package j_applet;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JApplet;
import javax.swing.JPanel;

public class J_Billentyu extends JApplet implements KeyListener{
    JPanel panel;
    
    public void init() {   
        addKeyListener(this);
        setFocusable(true);
    }

    @Override
    public void paint(Graphics g) {
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {}
}
