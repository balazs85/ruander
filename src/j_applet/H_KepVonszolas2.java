package j_applet;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JApplet;

public class H_KepVonszolas2 extends JApplet implements MouseListener, MouseMotionListener {

    Deque<H_SajatKep> alakzatok;
    H_SajatKep kivalasztott;
    Point holKlikkelt;

    public void init() {
        alakzatok = new LinkedList();
        alakzatok.add(new H_SajatKep(new Point(10, 10), "res/csillag.png"));
        alakzatok.add(new H_SajatKep(new Point(60, 10), "res/hangjegy.png"));
        alakzatok.add(new H_SajatKep(new Point(110, 10), "res/haz.png"));   //nem tökéletes az áttetszőség
        alakzatok.add(new H_SajatKep(new Point(160, 10), "res/ora.png"));
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        for (H_SajatKep alakzat : alakzatok) {
            alakzat.kirajzol(g);
        }
        if (kivalasztott != null) {
            kivalasztott.kirajzol(g);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //visszafelé kell nézni, mert azok vannak felül
        Point hely = e.getPoint();
        Iterator<H_SajatKep> iterator = alakzatok.descendingIterator();
        while (iterator.hasNext() && !(kivalasztott = iterator.next()).talalt(hely)) {

        }
        if (!kivalasztott.talalt(hely)) {
            kivalasztott = null;
        } else {
            alakzatok.remove(kivalasztott);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (kivalasztott != null) {
            alakzatok.add(kivalasztott);
            kivalasztott = null;
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (kivalasztott != null) {
            holKlikkelt = e.getPoint();
            kivalasztott.kirajzol(this.getGraphics(), holKlikkelt);
            repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
