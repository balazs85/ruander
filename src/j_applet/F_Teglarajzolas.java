package j_applet;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JApplet;

/**
 *
 * @author nbalazs
 */
public class F_Teglarajzolas extends JApplet 
    implements MouseMotionListener, MouseListener {

    Point kezdopont;
    Point vegpont;

    @Override
    public void init() {
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth()-1, getHeight()-1);
        if (kezdopont != null){
            int x = Math.min(kezdopont.x, vegpont.x);
            int y = Math.min(kezdopont.y, vegpont.y);
            int magassag = Math.abs(vegpont.x-kezdopont.x);
            int szelesseg = Math.abs(vegpont.y-kezdopont.y);
            
            g.drawRect(x, y, magassag, szelesseg);        
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        kezdopont = e.getPoint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        vegpont = e.getPoint();
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
