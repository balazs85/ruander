package j_applet;

import java.awt.Graphics;
import javax.swing.JApplet;

public class A_Hello extends JApplet {

    public void init() {
        // TODO start asynchronous download of heavy resources
    }

    @Override
    public void paint(Graphics g) {
        g.drawString("Hello World", 25, 50);
    }   
}
