package j_applet;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class H_SajatKep{
    private Image kep;
    private BufferedImage kepBG;
    private Point poz;
    private Point holAzEgerHozzamKepest = null;
    
    public H_SajatKep(Point poz, String utvonal) {
        this.poz = poz;
        try {
            kepBG = ImageIO.read(new File(utvonal));
            kep = new ImageIcon(utvonal).getImage();
        } catch (IOException ex) {
        }
    }
    
    public Image getKep(){
        return kep;
    }
    
    public boolean talalt(Point point) {    
        int x = point.x-poz.x;
        int y = point.y-poz.y;
        try {
            if (kepBG.getRGB(x, y)!=0){            //0 -> áttetszőség
                holAzEgerHozzamKepest = new Point(point.x-poz.x, point.y-poz.y);
                return true;
            }
        } catch (Exception e) {
        }        
        return false;
    }

    public void kirajzol(Graphics g) {
        g.drawImage(kep, poz.x, poz.y, null);
    }
    
    public void kirajzol(Graphics g, Point holAzEger) {
        poz.x = holAzEger.x-holAzEgerHozzamKepest.x;
        poz.y = holAzEger.y-holAzEgerHozzamKepest.y;
    }
}
