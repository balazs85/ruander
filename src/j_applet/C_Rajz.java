package j_applet;

import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JApplet;

public class C_Rajz extends JApplet {
    
    Point a;
    Point b;
    
    public void init () {
        String ax = getParameter("ax");
        String ay = getParameter("ay");
        String bx = getParameter("bx");
        String by = getParameter("by");
        a = new Point(Integer.parseInt(ax), Integer.parseInt(ay));
        b = new Point(Integer.parseInt(bx), Integer.parseInt(by)); 
    }
    public void paint (Graphics g) {
        g.drawRect(a.x, a.y, b.x-a.x, b.y-a.y);
    }
}
    