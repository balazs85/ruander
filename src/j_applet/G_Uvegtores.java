package j_applet;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JApplet;

public class G_Uvegtores extends JApplet implements MouseListener {

    //ArrayList<Point> pontok;
    Point aktualis;
    Image kep;

    public void init() {
        addMouseListener(this);
        kep = new ImageIcon("res/brokenKis2.png").getImage();
        //pontok = new ArrayList();
    }

    @Override
    public void paint(Graphics g) {
        if (aktualis != null) {
            g.drawImage(kep, aktualis.x, aktualis.y, null);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //pontok.add(e.getPoint());
        aktualis = new Point(e.getX() - 26, e.getY() - 23);
        audio("res/Noise.wav");
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private void audio(String utvonal) {
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream is = AudioSystem.getAudioInputStream(new File(utvonal));
            clip.open(is);
            clip.start();
        } catch (LineUnavailableException lineUnavailableException) {
        } catch (UnsupportedAudioFileException unsupportedAudioFileException) {
        } catch (IOException iOException) {
        }
    }

}
