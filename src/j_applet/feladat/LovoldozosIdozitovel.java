package j_applet.feladat;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JApplet;
import javax.swing.Timer;

public class LovoldozosIdozitovel extends JApplet implements 
        MouseMotionListener, MouseListener, ActionListener {
    
    Timer t;
    Point aktualis;
    ArrayList<Ember> ellensegek;
    //Ember kep;

    public void init() {
        ellensegek = new ArrayList();
        addMouseMotionListener(this);
        addMouseListener(this);
        
        t = new Timer(2000, this);
        t.start();
    }

    @Override
    public void start() {}

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());

        if (aktualis != null) {
            for (Ember kep : ellensegek) {
                g.drawImage(Ember.getKep(), kep.getPoz().x, kep.getPoz().y, null);
                g.drawLine(aktualis.x - 10, aktualis.y, aktualis.x + 10, aktualis.y);
                g.drawLine(aktualis.x, aktualis.y - 10, aktualis.x, aktualis.y + 10);
                g.drawOval(aktualis.x - 5, aktualis.y - 5, 11, 11);
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        aktualis = e.getPoint();
        repaint();
    }

    private void ujEllenseg() {
        Graphics g = this.getGraphics();
        Ember kep;
        //if (kep == null) {
            Random r = new Random();
            int x = r.nextInt(getWidth() - Ember.getKep().getHeight(null));
            int y = r.nextInt(getHeight() - Ember.getKep().getWidth(null));
            kep = new Ember(new Point(x, y));
            g.drawImage(kep.getKep(), kep.getPoz().x, kep.getPoz().y, null);
        //}
        ellensegek.add(kep);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Ember eltavolitando = null;
        for (Ember kep : ellensegek) {
            if (kep.talalt(e.getPoint())){
                eltavolitando = kep;
            }
        }
        if (eltavolitando != null){
            ellensegek.remove(eltavolitando);
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void actionPerformed(ActionEvent e) {
        ujEllenseg();
    }
}
