package j_applet.feladat;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import javax.swing.JApplet;
import javax.swing.JPanel;

public class Labirintus extends JApplet implements KeyListener{
    JPanel panel;
    
    int[] kodok = new int[41];
    int[][] palya;
    int cellaSzelesseg = 30;
    int kezdoX = 10;
    int kezdoY = 10;
    int jatekosX;
    int jatekosY;
    
    public void init() {
        kodok[38] = 3;
        kodok[37] = 2;
        kodok[40] = 1;
        kodok[39] = 0;
        
        palyaBetoltes("palya.txt");
        panel = new JPanel();
        this.add(panel);
        panel.addKeyListener(this);
        panel.setFocusable(true);
    }
    
    @Override
    public void paint(Graphics g) {
        palyaRajzolas(g);
    }
    
    private void palyaRajzolas(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        boolean[] szamjegyek;
        for (int i = 0; i < palya.length; i++) {
            for (int j = 0; j < palya[i].length; j++) {
                int szam = palya[i][j];
                szamjegyek = egeszbolLogTomb(szam);
                cellaRajzol(szamjegyek, i, j, g);
                emberrajzol(g);
            }
        }
    }

    private void emberrajzol(Graphics g) {
        g.setColor(Color.YELLOW);
        g.fillRect(kezdoX + cellaSzelesseg * jatekosX + 2, 
                kezdoY + cellaSzelesseg * jatekosY + 2, 
                cellaSzelesseg -4, cellaSzelesseg - 4);
        g.setColor(Color.BLACK);
    }    
    
    private void cellaRajzol(boolean[] szamjegyek, int sor, int oszlop, Graphics g) {
        int ujKezdoX = kezdoX + oszlop * cellaSzelesseg;
        int ujKezdoY = kezdoY + sor * cellaSzelesseg;
        if (szamjegyek[3]) g.drawLine(ujKezdoX, ujKezdoY, ujKezdoX + cellaSzelesseg, ujKezdoY);
        if (szamjegyek[2]) g.drawLine(ujKezdoX, ujKezdoY, ujKezdoX, ujKezdoY + cellaSzelesseg);
        if (szamjegyek[1]) g.drawLine(ujKezdoX, ujKezdoY + cellaSzelesseg, ujKezdoX + cellaSzelesseg, ujKezdoY + cellaSzelesseg);
        if (szamjegyek[0]) g.drawLine(ujKezdoX + cellaSzelesseg, ujKezdoY, ujKezdoX + cellaSzelesseg, ujKezdoY + cellaSzelesseg);
    }
    
    private boolean[] egeszbolLogTomb(int szam) {
        boolean[] szamjegyek = new boolean[4];
        for (int k = 0; k < 4; k++) {
            if (szam % 2 == 0) {
                szamjegyek[k] = false;
            } else {
                szamjegyek[k] = true;
            }
            szam /= 2;
        }
        return szamjegyek;
    }
    
    private void palyaBetoltes(String palyaUtvonal) {
        Scanner s = null;
        
        try {
            s = new Scanner(new BufferedReader(new FileReader(palyaUtvonal)));
            int sorokSzama = s.nextInt();
            int oszlopokSzama = s.nextInt();
            palya = new int[sorokSzama][oszlopokSzama];
            for (int i = 0; i < sorokSzama; i++) {
                for (int j = 0; j < oszlopokSzama; j++) {
                    palya[i][j] = s.nextInt();
                }
            }
            jatekosY = s.nextInt();
            jatekosX = s.nextInt();
        } catch (FileNotFoundException ex) {
            System.out.println("Nincs ilyen fájl");
        } finally {
            if (s != null) {
                s.close();
            }
        }
        
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        int lenyomva = e.getKeyCode();
        //fel  -> 38
        //bal  -> 37
        //le   -> 40
        //jobb -> 39
        int irany = kodok[lenyomva];
        if (!egeszbolLogTomb(palya[jatekosY][jatekosX])[irany]){
            switch (irany){
                case 0: jatekosX++; break;
                case 1: jatekosY++; break;
                case 2: jatekosX--; break;
                case 3: jatekosY--; break;    
            }
        }
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }


}
