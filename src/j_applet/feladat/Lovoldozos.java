package j_applet.feladat;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;
import javax.swing.JApplet;

public class Lovoldozos extends JApplet implements MouseMotionListener, MouseListener {

    Point aktualis;
    Ember kep;

    public void init() {
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    @Override
    public void start() {}

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        if (kep == null) {
            ujAldozat();
        }
        if (aktualis != null) {
            g.drawImage(Ember.getKep(), kep.getPoz().x, kep.getPoz().y, null);
            g.drawLine(aktualis.x - 10, aktualis.y, aktualis.x + 10, aktualis.y);
            g.drawLine(aktualis.x, aktualis.y - 10, aktualis.x, aktualis.y + 10);
            g.drawOval(aktualis.x - 5, aktualis.y - 5, 11, 11);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {
        aktualis = e.getPoint();
        repaint();
    }

    private void ujAldozat() {
        Graphics g = this.getGraphics();
        if (kep == null) {
            Random r = new Random();
            int x = r.nextInt(getWidth() - Ember.getKep().getHeight(null));
            int y = r.nextInt(getHeight() - Ember.getKep().getWidth(null));
            kep = new Ember(new Point(x, y));
            g.drawImage(kep.getKep(), kep.getPoz().x, kep.getPoz().y, null);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (kep.talalt(e.getPoint())){
            kep = null;
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
