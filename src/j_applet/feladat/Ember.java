package j_applet.feladat;

import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Ember{
    
    private static Image kep = new ImageIcon("res/Duke.gif").getImage();
    private BufferedImage kepBG = null;
    private Point poz;

    public Point getPoz() {
        return poz;
    }

    public Ember(Point poz) {
        this.poz = poz;
        try {
            kepBG = ImageIO.read(new File("res/Duke.gif"));
            
        } catch (IOException ex) {
        }
    }

    public static Image getKep(){
        return kep;
    }

    public boolean talalt(Point point) {       
        int x = point.x-poz.x;
        int y = point.y-poz.y;
        try {
            if (kepBG.getRGB(x, y)!=-1){
                return true;
            }
        } catch (Exception e) {
            
        }        
        return false;
    }
}
