/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package j_applet;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.JApplet;

/**
 *
 * @author nbalazs
 */
public class E_EgerkoordinatakKijelzese extends JApplet 
    implements MouseMotionListener {

    Point eger;

    @Override
    public void init() {
        addMouseMotionListener(this);
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, 100, 40);
        if (eger != null){
            g.drawRect(10, 10, 100, 20);
            g.drawString("x: "+eger.x+", y: "+eger.y, 14, 25);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        eger=new Point(e.getX(), e.getY());
        repaint();    
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

}
