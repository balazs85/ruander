package c_Tombkezeles.a_Tomb;

public class MatrixB {
    public static void main(String[] args) {
        int[][] m = {{10000, 0, 5000, 8000, 20000, 0, 0}, 
                     {2300, 1200, 2000, 1111, 12000, 23000, 2000}};
        for (int[] sor : m) {
            for (int elem : sor) {
                System.out.print(elem + "\t");
            }
            System.out.println();
        }
    }
}
