package c_Tombkezeles.b_Stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class C_Sorkezeles {
    public static void main(String[] args) throws IOException{
        String[] sorok = {"Alma", "Körte", "Barack" };
        PrintWriter pw = new PrintWriter(new FileWriter("sor.txt"));
        for (int i = 0; i < sorok.length; i++) {
             pw.println(sorok[i]);
        }
        pw.close();

        BufferedReader br = new BufferedReader(new FileReader("sor.txt"));
        String sor;
        while ((sor = br.readLine()) != null) {            
            System.out.println(sor);
        }
        br.close();
    }
}
