package c_Tombkezeles.b_Stream;

import java.io.FileReader;
import java.io.IOException;

public class B_OlvasasFajlbol {
    public static void main(String[] args) throws IOException{
        FileReader fr = new FileReader("elso.txt");
        int c;
        String sz="";
        while ((c = fr.read()) != -1) {
            sz = sz+(char)c;
        }
        System.out.println(sz);
        fr.close();
    }
}
