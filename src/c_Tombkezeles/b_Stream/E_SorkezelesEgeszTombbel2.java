package c_Tombkezeles.b_Stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import sun.misc.IOUtils;

public class E_SorkezelesEgeszTombbel2 {
    public static void main(String[] args) throws IOException{
        int[] t = {43, 77, 43, 12};
        FileWriter fw = new FileWriter("sz.txt");
        for (int i = 0; i < t.length; i++) {
             fw.write(t[i] + ";");
        }
        fw.close();

        BufferedReader br = new BufferedReader(new FileReader("sz.txt"));
        String sor = br.readLine();
        String[] darabok = sor.split(";");
        int[] szamok = new int[darabok.length];
        for (int i = 0; i < szamok.length; i++) {
            szamok[i] = Integer.parseInt(darabok[i]);
        }
        
        for (int j = 0; j < szamok.length; j++) {
            System.out.println(szamok[j]);
        }
    }
}
