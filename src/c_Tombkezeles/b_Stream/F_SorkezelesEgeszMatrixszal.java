package c_Tombkezeles.b_Stream;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class F_SorkezelesEgeszMatrixszal {
    public static void main(String[] args) throws IOException{
        int[][] m = {{43, 77, 43, 12}, 
                     {56, 22, 78, 34}};
        FileWriter fw = new FileWriter("sz.csv");
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                fw.write(m[i][j] + ";");
            }
            fw.write("\n");
        }
        fw.close();

        String egeszSzoveg = new String(Files.readAllBytes(Paths.get("sz.csv")));
        String[] sorok = egeszSzoveg.split("\n");
        String[] egysor = sorok[0].split(";");
        int[][] m2 = new int [sorok.length][egysor.length];
        for (int i = 0; i < sorok.length; i++) {
            egysor = sorok[i].split(";");
            for (int j = 0; j < egysor.length; j++) {
                m2[i][j] = Integer.parseInt(egysor[j]);
            }

        }
        
        for (int i = 0; i < m2.length; i++) {
            for (int j = 0; j < m2[i].length; j++) {
                System.out.print(m2[i][j] + "\t");
            }
            System.out.println("");
        }
    }
}
