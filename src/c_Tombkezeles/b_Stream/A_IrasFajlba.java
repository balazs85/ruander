package c_Tombkezeles.b_Stream;

import java.io.FileWriter;
import java.io.IOException;

public class A_IrasFajlba {
    public static void main(String[] args) throws IOException{
        String szoveg = "Hello World\nAlmafa";
        FileWriter fw = new FileWriter("elso.txt");
        fw.write(szoveg);
        fw.close();
    }
}
