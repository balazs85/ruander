/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p_kerteszet;

import java.sql.Date;

/**
 *
 * @author nbalazs
 */
public class Munka {
    private Integer azon;
    private String nev;
    private String e_mail;
    private Date kezdet; 
    private Date veg;
    private Integer ar;

    public Munka() {
    }

    public Munka(Integer azon, Date kezdet, Date veg, Integer ar, String nev, String e_mail) {
        this.azon = azon;
        this.kezdet = kezdet;
        this.veg = veg;
        this.ar = ar;
        this.nev = nev;
        this.e_mail = e_mail;
    }

    public Integer getAzon() {
        return azon;
    }

    public void setAzon(Integer azon) {
        this.azon = azon;
    }

    public Date getKezdet() {
        return kezdet;
    }

    public void setKezdet(Date kezdet) {
        this.kezdet = kezdet;
    }

    public Date getVeg() {
        return veg;
    }

    public void setVeg(Date veg) {
        this.veg = veg;
    }

    public Integer getAr() {
        return ar;
    }

    public void setAr(Integer ar) {
        this.ar = ar;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    @Override
    public String toString() {
        return kezdet.toString() + " - " + veg.toString();
    }
    
    
}
