/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p_kerteszet;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author nbalazs
 */
public class Adatbazis {

    private String dbFajl;

    public Adatbazis() {
        dbFajl = "res/sqlite/kerteszet.db";

        if (!isTableExists("TMunka")) {
            createMunka();
        }

    }

    private boolean isTableExists(String tablanev) {
        Connection c = null;
        boolean result = false;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            DatabaseMetaData dbm = c.getMetaData();
            ResultSet tables = dbm.getTables(null, null, tablanev, null);
            if (tables.next()) {
                result = true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }
        return result;
    }

    private void modositasFuttatasa(String sql) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void createMunka() {
        String sql = "CREATE TABLE TMunka ("
                + "azon INTEGER PRIMARY KEY, "
                + "nev TEXT NOT NULL, "
                + "e_mail TEXT NOT NULL, "
                + "kezdet DATE, "
                + "veg DATE, "
                + "ar INTEGER);";

        Connection c = null;
        Statement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.createStatement();
            s.executeUpdate(sql);
            s.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    Integer saveOrUpdate(Munka m) {
        if (m.getAzon()==null){
            save(m);
        } else {
            update(m);
        }
        return null;
    }

    private void update(Munka m) {
        String sql = "UPDATE TMunka SET "
                + "nev=?, "
                + "e_mail=?, "
                + "kezdet=?, "
                + "veg=?, "
                + "ar=? "
                + "WHERE azon=?";
        
        Connection c = null;
        PreparedStatement s = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.prepareStatement(sql);
            s.setString(1, m.getNev());
            s.setString(2, m.getE_mail());
//SQLite nem tud datumot kezelni rendesen
//            s.setDate(3, m.getKezdet());
//            s.setDate(4, m.getVeg());
            s.setString(3, m.getKezdet().toString());
            s.setString(4, m.getVeg().toString());
            s.setInt(5, m.getAr());
            s.setInt(6, m.getAzon());
            s.executeUpdate();
            s.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }  
    }

    private void save(Munka m) {
        String sql = "INSERT INTO TMunka VALUES (null, ?, ?, ?, ?, ?)";
        
        Connection c = null;
        PreparedStatement s = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.prepareStatement(sql);
            s.setString(1, m.getNev());
            s.setString(2, m.getE_mail());
            s.setString(3, m.getKezdet().toString());
            s.setString(4, m.getVeg().toString());
            s.setInt(5, m.getAr());
            s.executeUpdate();
            s.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }  

    }

    public ArrayList<Munka> selectMunkak() {
        ArrayList<Munka> elemek = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String sql="SELECT * FROM TMunka ORDER BY nev";
        Connection c = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            stmt = c.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                Munka m = new Munka();
                m.setAzon(result.getInt(1));
                m.setNev(result.getString(2));
                m.setE_mail(result.getString(3));
//ASLite miatt nem megy
//                m.setKezdet(result.getDate(4));
//                m.setVeg(result.getDate(5));
                m.setKezdet(new Date(df.parse(result.getString(4)).getTime()));
                m.setVeg(new Date(df.parse(result.getString(5)).getTime()));
                m.setAr(result.getInt(6));
                elemek.add(m);
            }

            c.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return elemek;
    }

    void delete(Munka m) {
        String sql = "DELETE FROM TMunka WHERE azon=?";
        
        Connection c = null;
        PreparedStatement s = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.prepareStatement(sql);
            s.setInt(1, m.getAzon());
            s.executeUpdate();
            s.close();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }  
    }


}
