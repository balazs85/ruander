package l_GrafikusFelulet.Feladat1_Panasz;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Panasz implements Serializable{
    private String leiras;
    private Date panaszDatuma;
    private Boolean megoldva;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd.");

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    public Panasz(String leiras, Date panaszDatuma, Boolean megoldva) {
        this.leiras = leiras;
        this.panaszDatuma = panaszDatuma;
        this.megoldva = megoldva;
    }

    public String getLeiras() {
        return leiras;
    }

    public Date getPanaszDatuma() {
        return panaszDatuma;
    }

    public Boolean getMegoldva() {
        return megoldva;
    }

    @Override
    public String toString() {
        return dateFormat.format(panaszDatuma);
    }
    
    
}
