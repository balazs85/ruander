package l_GrafikusFelulet.Feladat1_Panasz;

import java.io.Serializable;
import java.util.ArrayList;

public class Ugyfel implements Serializable{
    private String nev;
    private String ugyfelszam;
    private ArrayList<Panasz> panaszai;

    public Ugyfel(String nev, String ugyfelszam) {
        this.nev = nev;
        this.ugyfelszam = ugyfelszam;
        this.panaszai = new ArrayList<>();
    }

    public String getNev() {
        return nev;
    }

    public String getUgyfelszam() {
        return ugyfelszam;
    }

    public ArrayList<Panasz> getPanaszai() {
        return panaszai;
    }

    public void addPanasz(Panasz p){
        panaszai.add(p);
    }
    
    public Panasz modPanasz(int index, Panasz p){
        return panaszai.set(index, p);
    }
    
    public Panasz removePanasz(int index){
        return panaszai.remove(index);
    }
    
    @Override
    public String toString() {
        return nev;
    }
}
