package l_GrafikusFelulet;

public class F_DiakOsztaly {
    private String nev;
    private Integer magassag;

    public F_DiakOsztaly(String nev, int magassag) {
        this.nev = nev;
        this.magassag = magassag;
    }

    public F_DiakOsztaly(String sor) {
        this.nev = sor.split(",")[0];
        this.magassag = Integer.parseInt(sor.split(",")[1]);
    }
    
    public String getNev() {
        return nev;
    }

    public Integer getMagassag() {
        return magassag;
    }

    @Override
    public String toString() {
        return getNev();
    }
    
    
}
