package m_Halozat;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class D_Level {
    public static void main(String[] args) {
        sendMessage2("balazs85@gmail.com", 
                "balazs85@gmail.com", 
                "Ezt egy java programból küldtem");
    }
    
    private static void sendMessage(String kitol, String kinek, String szoveg) {
        Properties beallitasok = System.getProperties();
        beallitasok.setProperty("mail.smtp.host", "smtp.upcmail.hu");
        Session session = Session.getDefaultInstance(beallitasok);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(kitol));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(kinek));
            message.setSubject("Ezt mindenképp olvasd el!");
            message.setText(szoveg);
            Transport.send(message);
            System.out.println("Sikerült elküldeni");
        } catch (MessagingException messagingException) {
            System.out.println(messagingException.getMessage());
        }
        
    }
    
    private static void sendMessage2(String kitol, String kinek, String szoveg) {
        String username = "teszt@elek.hu";
        String password = "Almafa12";

        Properties beallitasok = System.getProperties();
        beallitasok.put("mail.smtp.auth", "true");
        beallitasok.put("mail.smtp.starttls.enable", "true");
        beallitasok.put("mail.smtp.host", "smtp.gmail.com");
        beallitasok.put("mail.smtp.port", "587");
        Session session = Session.getDefaultInstance(beallitasok,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(kitol));
            message.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(kinek));
            message.setSubject("Ezt mindenképp olvasd el!");
            message.setText(szoveg);
            Transport.send(message);
            System.out.println("Sikerült elküldeni");
        } catch (MessagingException messagingException) {
            System.out.println(messagingException.getMessage());
        }
        
    }
}
