package m_Halozat;

import java.net.*;
import java.io.*;


public class A_VisszhangSzerver {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        Socket clientSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        int port = 11111;
        
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.err.println("Nem hallgatózhatom a következő porton: "+port+".");
            System.exit(1);
        }
        
        
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.err.println("Nem sikerült a kapcsolat az elfogadása.");
            System.exit(1);
        }
        
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            out.println(inputLine);
        }
        
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

}

