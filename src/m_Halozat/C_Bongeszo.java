package m_Halozat;

import java.net.*;
import java.io.*;

public class C_Bongeszo {

    public static void main(String[] args) throws IOException {
        String cim = "www.hwsw.hu";
        int port = 80;
        Socket s = new Socket(cim, port);
        InputStream is = s.getInputStream();
        OutputStream os = s.getOutputStream();
        PrintWriter out = new PrintWriter(
                new OutputStreamWriter(os));
        BufferedReader in = new BufferedReader(
                new InputStreamReader(is));
        out.println("GET / HTTP/1.1");
        out.println("Accept:text/html");
        //        out.println("Accept-Encoding:gzip,deflate,sdch");
        //        out.println("Accept-Language:hu-HU,hu;q=0.8,en-US;q=0.6,en;q=0.4");
        //        out.println("Connection:keep-alive");
        out.println("Host: "+cim);
        //        out.println("User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36");

        out.println();
        out.flush();
        String line;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
        out.close();
        s.close();
    }
}
