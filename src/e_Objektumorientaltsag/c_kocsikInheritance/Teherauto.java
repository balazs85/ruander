package e_Objektumorientaltsag.c_kocsikInheritance;

public class Teherauto extends Auto2{

    private int mennyiSulytVihet;
            
    public Teherauto(String rendszam, int tartalyKapacitas, double tartalySzint, int fogyasztas, int sulytVihet) {
        super(rendszam, tartalyKapacitas, tartalySzint, fogyasztas);
        mennyiSulytVihet = sulytVihet;
    }
    
    public int getMennyiSulytVihet(){
        return mennyiSulytVihet;
    }
    
    @Override
    public String toString(){
        return super.toString()+", ami egy teherautó";
    }
}
