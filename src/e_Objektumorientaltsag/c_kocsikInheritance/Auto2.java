package e_Objektumorientaltsag.c_kocsikInheritance;

import e_Objektumorientaltsag.a_kocsik.*;

public class Auto2 {
    private static int literenkentiAr = 320;
    private String rendszam;
    private int tartalyKapacitas;
    private double tartalySzint;
    private int fogyasztas;     //l 100km-nként
    
    public Auto2(String rendszam, int tartalyKapacitas, 
            double tartalySzint, int fogyasztas){
        this.rendszam = rendszam;
        this.tartalyKapacitas = tartalyKapacitas;
        this.tartalySzint = tartalySzint;
        this.fogyasztas = fogyasztas;
    }
    
    public static void benzinArValtozas(int ujErtek){
        literenkentiAr = ujErtek;
    }
    
    public double hanyLitertFogFogyasztani(int km){
        return (double)fogyasztas / 100 * km;
    }
    
    public double mennyibeKerul(int km){
        return hanyLitertFogFogyasztani(km) * literenkentiAr;
    }
    
    @Override
    public String toString(){
        return "("+rendszam+") Egy autó vagyok";
    }
}
