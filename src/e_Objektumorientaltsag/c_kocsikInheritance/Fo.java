package e_Objektumorientaltsag.c_kocsikInheritance;

import e_Objektumorientaltsag.a_kocsik.*;

public class Fo {
    
    public static void main(String[] args){
        Auto enyem;
        enyem = new Auto("ABC-123", 55, 40, 8);
        System.out.println(enyem.hanyLitertFogFogyasztani(90));
        //-----------------------------------------------------
        Auto2 sportautom = new Auto2("SPEED-1", 60, 40, 18);
        Auto2 bevasarlokocsim = new Auto2("BOR-111", 40, 30, 5);
        System.out.println(sportautom.mennyibeKerul(80));
        System.out.println(bevasarlokocsim.mennyibeKerul(80));

        Auto2.benzinArValtozas(325);
        System.out.println(sportautom.mennyibeKerul(80));
        System.out.println(bevasarlokocsim.mennyibeKerul(80));
        
        Teherauto teher = new Teherauto("TTT-100", 300, 200, 19, 2);
        Auto2 teher2 = new Teherauto("TTT-101", 300, 160, 19, 2);
        System.out.println(teher.mennyibeKerul(80));
        System.out.println(teher2.mennyibeKerul(80));
        System.out.println(teher.getMennyiSulytVihet());
        System.out.println(((Teherauto)teher2).getMennyiSulytVihet());

        Auto2[] autok = {sportautom, bevasarlokocsim, teher, teher2};
        System.out.println("-------------------------");
        for (int i = 0; i < autok.length; i++) {
            System.out.println(autok[i].toString());
        }
    }
}
