package e_Objektumorientaltsag.feladat_diak;

public class Diak {
    private String nev;
    private double magassag;
    private int testsuly;
    private static int db = 0;

    public Diak(String nev, double magassag, int testsuly) {
        this.nev = nev;
        this.magassag = magassag;
        this.testsuly = testsuly;
        this.db = this.db + 1;
    }
    
    public static void diakdb() {
        System.out.println("A diákok száma: " + (db));
    }
    
}