package e_Objektumorientaltsag.feladat_diak;

public class Fo {
    public static void main(String[] args) {
        String[] nev = {"Dani", "Bela", "Jani"};
        
        //1. lehetőség
        Diak Dani; //csak kipróbálom mind a két lehetõséget
        Dani = new Diak("Dani", 1.75, 90);
        Diak Bela = new Diak("Bela", 1.84, 95);
        Diak Jani = new Diak("Jani", 1.78, 72);
        Diak[] diakok = {Dani, Bela, Jani};
        
        //2. lehetőség
        Diak[] masikDiakok = new Diak[3];
        masikDiakok[0] = new Diak("Dani", 1.75, 90);
        masikDiakok[1] = new Diak("Bela", 1.84, 95);
        masikDiakok[2] = new Diak("Jani", 1.78, 72);
        
        
        Diak.diakdb();
    }
}
