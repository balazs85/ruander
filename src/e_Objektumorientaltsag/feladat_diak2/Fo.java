package e_Objektumorientaltsag.feladat_diak2;

public class Fo {

    public static void main(String[] args) {
        Szemely akarki = new Szemely("Senki Alfonz", 1.83, 74);
        Szemely en = new Tanar("Nagy Balázs Tamás", 1.7, 60, "6. b, 7. b, 8. b, 9. A, 9. KNY, 10. A");
        Diak d1 = new Diak("Kovács Emese", 1.5, 35, "6. b");
        Diak d2 = new Diak("Kiss István", 1.68, 50, "8. b");

        Szemely[] szemelyek = {akarki, en, d1, d2};
        for (int i = 0; i < szemelyek.length; i++) {
            System.out.println(szemelyek[i]);
        }

        System.out.print("Problémás testtömegindexűek: ");
        Szemely[] nemNormalisak = kivalaogatInverz(szemelyek, "normális testsúly");
        if (nemNormalisak.length > 0) {
            for (int i = 0; i < nemNormalisak.length - 1; i++) {
                System.out.print(nemNormalisak[i].getNev() + ", ");
            }
            System.out.println(nemNormalisak[nemNormalisak.length - 1].getNev());
        } else {
            System.out.println("nincs ilyen");
        }
    }

    private static Szemely[] kivalaogatInverz(Szemely[] szemelyek, String sz) {
        Szemely[] kivalogatottak = new Szemely[szemelyek.length];
        int db = 0;
        for (int i = 0; i < szemelyek.length; i++) {
            if (!szemelyek[i].jellemzes().equals(sz)) {
                kivalogatottak[db] = szemelyek[i];
                db++;
            }
        }
        return java.util.Arrays.copyOf(kivalogatottak, db);
    }
}
