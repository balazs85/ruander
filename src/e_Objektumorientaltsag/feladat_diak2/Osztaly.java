package e_Objektumorientaltsag.feladat_diak2;

public class Osztaly {

    private int szam;
    private String szoveg;

    public Osztaly(int szam, String szoveg) {
        this.szam = szam;
        this.szoveg = szoveg;
    }

    public Osztaly(String szoveg) {
        String[] darabok = szoveg.split(". ");
        this.szam = Integer.parseInt(darabok[0]);
        this.szoveg = darabok[1];
    }

    public void novel() {
        szam++;
    }

    @Override
    public String toString() {
        return szam + ". " + szoveg;
    }

    public static Osztaly[] StringToOsztalyTomb(String[] osztalySzovegek) {
        Osztaly[] osztalyok = new Osztaly[osztalySzovegek.length];
        for (int i = 0; i < osztalyok.length; i++) {
            osztalyok[i] = new Osztaly(osztalySzovegek[i]);
        }
        return osztalyok;
    }

    public static Osztaly[] StringToOsztalyTomb(String osztalyokSzoveg) {
        return StringToOsztalyTomb(osztalyokSzoveg.split(", "));
    }

}
