/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e_Objektumorientaltsag.feladat_diak2;

public class Tanar extends Szemely {

    Osztaly[] osztalyai;

    public Tanar(String nev, double magassag, int tomeg) {
        super(nev, magassag, tomeg);
    }

    public Tanar(String nev, double magassag, int tomeg, String osztalyai) {
        super(nev, magassag, tomeg);
        this.osztalyai = Osztaly.StringToOsztalyTomb(osztalyai);
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\tOsztályai: ";
        for (int i = 0; i < osztalyai.length - 1; i++) {
            result += osztalyai[i] + ", ";
        }
        result += osztalyai[osztalyai.length - 1] + "\n";
        return result;
    }
}
