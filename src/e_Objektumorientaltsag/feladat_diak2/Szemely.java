package e_Objektumorientaltsag.feladat_diak2;

public class Szemely {

    protected String nev;
    protected double magassag;
    protected int tomeg;
    protected static double[] max = {16, 17, 18.5, 25, 30, 35, 40, 10000};
    protected static String[] jellemzes = {"súlyos soványság", "mérsékelt soványság",
        "enyhe soványság", "normális testsúly", "túlsúlyos",
        "I. fokú elhízás", "II. fokú elhízás", "III. fokú (súlyos) elhízás"};

    public String getNev() {
        return nev;
    }
    
    //Alt+Insert -> elkészíti a konstruktort
    public Szemely(String nev, double magassag, int tomeg) {
        this.nev = nev;
        this.magassag = magassag;
        this.tomeg = tomeg;
    }

    public double testtomegindex() {
        return (double) tomeg / (magassag * magassag);
    }

    public String jellemzes() {
        int i = 0;
        double index = testtomegindex();
        while (index > max[i]) {
            i++;
        }
        return jellemzes[i];
    }

    @Override
    public String toString() {
        String result = nev + " (" + this.getClass().getSimpleName() + "):\n";
        int szoveghossz = result.length() - 1;
        for (int i = 0; i < szoveghossz; i++) {
            result += "-";
        }
        result += "\n";
        result += "\tMagassága: " + magassag + "\n";
        result += "\tTesttömege: " + tomeg + "\n";
        result += "\tTesttömegindexe(" + String.format("%.2f", testtomegindex()) + ") alapján a besorolása: " + jellemzes() + "\n";
        return result;
    }

}
