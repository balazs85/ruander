package e_Objektumorientaltsag.feladat_diak2;

public class Diak extends Szemely {

    private Osztaly osztaly;

    public Diak(String nev, double magassag, int tomeg, String osztaly) {
        super(nev, magassag, tomeg);
        this.osztaly = new Osztaly(osztaly);
    }

    public void kovetkezoOsztalybaLep() {
        osztaly.novel();
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\tOsztálya: " + osztaly + "\n";
        return result;
    }

}
