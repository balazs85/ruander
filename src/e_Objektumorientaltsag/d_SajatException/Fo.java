package e_Objektumorientaltsag.d_SajatException;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fo {
public static void main(String[] args) {
    try {
        int eletkor = eletkorbeker();
        System.out.println(eletkor);
    } catch (RosszEletkorException ex) {
        System.out.println(ex.getMessage());
    } catch (java.util.InputMismatchException ex){
        System.out.println("Nem egész számot írt be életkornak");
    }
}

private static int eletkorbeker() throws RosszEletkorException {
    Scanner in = new Scanner(System.in);
    System.out.println("Adjon meg az életkorát!");
    int szam = in.nextInt();
    if (szam<0 || szam > 150){
        throw new RosszEletkorException();
    }
    return szam;
}
    
}
