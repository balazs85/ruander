package e_Objektumorientaltsag.b_Kivetelkezeles;

public class C_Tomb {

    public static void main(String[] args) {
        int[] t = {10, 55, 32};
        kiir(t);
    }

    private static void kiir(int[] t) {
        try {
            for (int i = 0; i < 10; i++) {
                System.out.println(t[i]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Kívülindexelt a tömbön");
        }
    }
}
