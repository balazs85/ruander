package e_Objektumorientaltsag.b_Kivetelkezeles;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class B_FajlMegnyitasa {

    public static void main(String[] args) {
        try {
            FileReader fr = new FileReader("elso.txt");
        } catch (FileNotFoundException ex) {
            System.out.println("Nem található a fájl");
        }
    }
}
