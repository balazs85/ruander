package e_Objektumorientaltsag.b_Kivetelkezeles;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class E_TobbHibaFinally {

    public static void main(String[] args) {
        kiir();
    }

    private static void kiir() {
        int[] szamok = {23, 55, 12};
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileWriter("ki.txt"));
            String sor;
            for (int i = 0; i < 10; i++) {
                pw.println(szamok[i]);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Kívülindexelt a tömbön");
        } catch (IOException ex) {
            System.out.println("Baj van a fájllal");
        } finally {
            if (pw != null){
                pw.close();
            }
        }
    }
}
