package e_Objektumorientaltsag.b_Kivetelkezeles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class D_TobbHiba {

    public static void main(String[] args) {
        kiir();
    }

    private static void kiir() {
        int[] szamok = new int[3];
        try {
            BufferedReader br = new BufferedReader(new FileReader("szamok.txt"));
            String sor;
            int i = 0;
            while ((sor = br.readLine()) != null) {
                szamok[i] = Integer.parseInt(sor);
                i++;
            }
            br.close();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Kívülindexelt a tömbön");
        } catch (IOException ex) {
            System.out.println("Baj van a fájllal");
        }
    }
}
