package e_Objektumorientaltsag.e_kocsikInterface;


public class Busz extends Auto2 implements ISzemelyszallito{

    private int szemelyekSzama;
    
    public Busz(String rendszam, int tartalyKapacitas, double tartalySzint, int fogyasztas, int szemelyekSzama) {
        super(rendszam, tartalyKapacitas, tartalySzint, fogyasztas);
        this.szemelyekSzama = szemelyekSzama;
    }

    @Override
    public String toString() {
        return super.toString() + ", ami egy busz";
    }

    @Override
    public int ulohely() {
        return szemelyekSzama;
    }
    
}
