package e_Objektumorientaltsag.e_kocsikInterface;

public class Fo {
    
    public static void main(String[] args){
        Szemelyauto enyem = new Szemelyauto("ABC-123", 55, 40, 8, 5, 500);
        Szemelyauto bevasarlokocsim = new Szemelyauto("BOR-111", 40, 30, 5, 3, 100);
        Auto2 sportautom = new Auto2("SPEED-1", 60, 40, 18);
        Teherauto teher = new Teherauto("TTT-100", 300, 200, 19, 2000);
        Teherauto teher2 = new Teherauto("TTT-101", 300, 160, 19, 2000);
        Busz sprinter = new Busz("BUS-001", 200, 150, 12, 18);
        Busz transporter = new Busz("BUS-002", 210, 120, 8, 8);
        
        Auto2[] autok = {enyem, sportautom, bevasarlokocsim, teher, teher2, sprinter, transporter};
        for (int i = 0; i < autok.length; i++) {
            System.out.println(autok[i]);
        }
        System.out.println("--------------------------------");
        
        int elszallitandoSzemely = 25;
        int elszallitandoKG = 3000;
        ISzemelyszallito[] viszemSzemely = new ISzemelyszallito[autok.length];
        ITeherhordo[] viszemTeher = new ITeherhordo[autok.length];
        int dbSzemely = 0;
        int dbTeher = 0;
        int i = 0;
        while (i < autok.length && elszallitandoKG>0 && elszallitandoSzemely>0) {
            if (autok[i] instanceof ISzemelyszallito){
                viszemSzemely[dbSzemely] = (ISzemelyszallito)autok[i];
                dbSzemely++;
                elszallitandoSzemely -= ((ISzemelyszallito)autok[i]).ulohely();
            }
            if (autok[i] instanceof ITeherhordo){
                viszemTeher[dbTeher] = (ITeherhordo)autok[i];
                dbTeher++;
                elszallitandoKG-= ((ITeherhordo)autok[i]).teherbiras();
            }
            i++;
        }
        while (i < autok.length && elszallitandoSzemely>0) {
            if (autok[i] instanceof ISzemelyszallito){
                viszemSzemely[dbSzemely] = (ISzemelyszallito)autok[i];
                dbSzemely++;
                elszallitandoSzemely -= ((ISzemelyszallito)autok[i]).ulohely();
            }
            i++;
        }
        while (i < autok.length && elszallitandoKG>0) {
            if (autok[i] instanceof ITeherhordo){
                viszemTeher[dbTeher] = (ITeherhordo)autok[i];
                dbTeher++;
                elszallitandoKG-= ((ITeherhordo)autok[i]).teherbiras();
            }
            i++;
        }
        
        System.out.println("Teherhordók:");
        for (int j = 0; j < dbTeher; j++) {
            System.out.println(viszemTeher[j]);
        }
        System.out.println("Személyszállítók:");
        for (int j = 0; j < dbSzemely; j++) {
            System.out.println(viszemSzemely[j]);
        }
        
    }
}
