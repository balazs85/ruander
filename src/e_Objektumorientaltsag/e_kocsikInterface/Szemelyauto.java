package e_Objektumorientaltsag.e_kocsikInterface;


public class Szemelyauto extends Auto2 implements ISzemelyszallito, ITeherhordo{

    private int szemelyekSzama;
    private int mennyiSulytVihet;    

    public Szemelyauto(String rendszam, int tartalyKapacitas, double tartalySzint, int fogyasztas, int szemelyekSzama, int mennyiSulytVihet) {
        super(rendszam, tartalyKapacitas, tartalySzint, fogyasztas);
        this.szemelyekSzama = szemelyekSzama;
        this.mennyiSulytVihet = mennyiSulytVihet;
    }
    
    @Override
    public String toString() {
        return super.toString() + ", ami egy személyautó";
    }
    
    @Override
    public int ulohely() {
        return szemelyekSzama;
    }
    
    @Override
    public int teherbiras() {
        return mennyiSulytVihet;
    }
    
    
}
