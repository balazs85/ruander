package e_Objektumorientaltsag.e_kocsikInterface;

public class Teherauto extends Auto2 implements ITeherhordo {

    private int mennyiSulytVihet;

    public Teherauto(String rendszam, int tartalyKapacitas, double tartalySzint, int fogyasztas, int sulytVihet) {
        super(rendszam, tartalyKapacitas, tartalySzint, fogyasztas);
        mennyiSulytVihet = sulytVihet;
    }

    @Override
    public String toString() {
        return super.toString() + ", ami egy teherautó";
    }

    @Override
    public int teherbiras() {
        return mennyiSulytVihet;
    }
}
