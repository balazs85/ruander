package e_Objektumorientaltsag.a_kocsik;

public class Auto {
    private String rendszam;
    private int tartalyKapacitas;
    private double tartalySzint;
    private int fogyasztas;     //l 100km-nként
    
    public Auto(String rendszam, int tartalyKapacitas, 
            double tartalySzint, int fogyasztas){
        this.rendszam = rendszam;
        this.tartalyKapacitas = tartalyKapacitas;
        this.tartalySzint = tartalySzint;
        this.fogyasztas = fogyasztas;
    }
    
    public double hanyLitertFogFogyasztani(int km){
        return (double)fogyasztas / 100 * km;
    }
}
