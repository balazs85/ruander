package e_Objektumorientaltsag.e_AllatInterface;

public class Fo {
    public static void main(String[] args){
        Husevo oroszlan = new Husevo();
        Novenyevo tehen = new Novenyevo();

        IAllat[] allatok = {oroszlan, tehen};

        for (int i = 0; i < allatok.length; i++) {
            allatok[i].eszik();
        }
    }
}
