package e_Objektumorientaltsag.feladat_racionalis;

class Racionalis {

    int szamlalo;
    int nevezo;

    public Racionalis(int szamlalo, int nevezo) throws ArithmeticException {
        if (nevezo == 0) {
            throw new ArithmeticException("A nevezo nem lehet 0");
        }
        this.szamlalo = szamlalo;
        this.nevezo = nevezo;
    }

    public Racionalis(int szamlalo) {
        this.szamlalo = szamlalo;
        this.nevezo = 1;
    }

    public Racionalis reciprok() {
        return new Racionalis(nevezo, szamlalo);
    }

    public Racionalis szorzas(Racionalis masik) {
        Racionalis eredmeny = new Racionalis(szamlalo * masik.szamlalo, nevezo * masik.nevezo);
        return eredmeny.egyszerusit();
    }

    public Racionalis osztas(Racionalis masik){
        return this.szorzas(masik.reciprok());
    }

    public Racionalis osszeadas(Racionalis masik){
        int lkkt = legkisebbKozosTobbszoros(nevezo, masik.nevezo);
        int eredmenySzamlalo = lkkt / nevezo * szamlalo + lkkt / masik.nevezo * masik.szamlalo; 
        int eredmenyNevezo =  lkkt;
        return (new Racionalis(eredmenySzamlalo, eredmenyNevezo)).egyszerusit();
    }

    public Racionalis kivonas(Racionalis masik){
        int lkkt = legkisebbKozosTobbszoros(nevezo, masik.nevezo);
        int eredmenySzamlalo = lkkt / nevezo * szamlalo - lkkt / masik.nevezo * masik.szamlalo; 
        int eredmenyNevezo =  lkkt;
        return (new Racionalis(eredmenySzamlalo, eredmenyNevezo)).egyszerusit();
    }
    
    public double valosAlak(){
        return (double)szamlalo / nevezo; 
    }
    
    public Racionalis egyszerusit() {
        int eredmenySzamlalo = szamlalo;
        int eredmenyNevezo = nevezo;
        int lnko;
        while ((lnko = legnagyobbKozosOszto(eredmenySzamlalo, eredmenyNevezo)) != 1) {
            eredmenySzamlalo = eredmenySzamlalo / lnko;
            eredmenyNevezo = eredmenyNevezo / lnko;
        }
        return new Racionalis(eredmenySzamlalo, eredmenyNevezo);
    }

    private int legkisebbKozosTobbszoros(int a, int b) {
        int x = a;
        int y = b;
        while (x != y) {
            if (x < y) {
                x += a;
            } else if (x > y) {
                y += b;
            }
        }
        return x;
    }

    private int legnagyobbKozosOszto(int a, int b) {
        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }
        int maradek = a % b;
        while (maradek > 0) {
            a = b;
            b = maradek;
            maradek = a % b;
        }
        return b;
    }

    @Override
    public String toString() {
        return szamlalo + "/" + nevezo;
    }

}
