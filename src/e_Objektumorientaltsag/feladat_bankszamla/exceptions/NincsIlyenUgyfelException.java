/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e_Objektumorientaltsag.feladat_bankszamla.exceptions;


public class NincsIlyenUgyfelException extends Exception {

    public NincsIlyenUgyfelException() {
        super("Nem létezik az ügyfél, akinek utalni akarsz");
    }
    
}
