package e_Objektumorientaltsag.feladat_bankszamla.exceptions;


public class NemLehetNegativ extends Exception {

    public NemLehetNegativ() {
        super("Nem adhat meg negatív értéket!");
    }
    
}
