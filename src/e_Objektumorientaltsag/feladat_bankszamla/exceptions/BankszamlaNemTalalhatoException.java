package e_Objektumorientaltsag.feladat_bankszamla.exceptions;

public class BankszamlaNemTalalhatoException extends Exception{

    public BankszamlaNemTalalhatoException() {
        super("Az ügyfélnek nincs ilyen bankszámlája");
    }
    
}
