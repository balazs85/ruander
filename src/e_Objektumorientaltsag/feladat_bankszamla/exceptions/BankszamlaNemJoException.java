package e_Objektumorientaltsag.feladat_bankszamla.exceptions;

public class BankszamlaNemJoException extends Exception{

    public BankszamlaNemJoException() {
        super("Nem jól formázott a bankszámla!");
    }
    
}
