package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.*;

public class Folyoszamla extends Szamla {

    public Folyoszamla(String haromNyolcasBlock, int osszeg) throws BankszamlaNemJoException {
        super(haromNyolcasBlock, osszeg);
    }

    public Folyoszamla(String haromNyolcasBlock) throws BankszamlaNemJoException {
        super(haromNyolcasBlock);
    }

    @Override
    public String toString() {
        return "Folyószámla " + super.toString();
    }
    
    void betesz(int osszeg) throws NemLehetNegativ {
        if (osszeg < 0){
            throw new NemLehetNegativ();
        }
        this.osszeg+=osszeg;
    }
    
    void kivesz(int osszeg) throws NincsFedezet {
        if (osszeg > this.osszeg){
            throw new NincsFedezet();
        }
        this.osszeg-=osszeg;
    }
    
}
