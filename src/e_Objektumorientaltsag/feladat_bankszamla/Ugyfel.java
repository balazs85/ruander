package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemTalalhatoException;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NemLehetNegativ;

public class Ugyfel {

    String nev;
    Szamla[] szamlai;

    public Szamla keresSzamlaSzamlaszamAlapjan(String szamlaszam){
        int sorsz = keresSzamlaszama(szamlaszam);
        if (sorsz == -1){
            return null;
        } else {
            return szamlai[sorsz];
        }
        
    }
    
    public boolean van_eSzamlaszama(String szamlaszam) {
        return keresSzamlaszama(szamlaszam) != -1;
    }

    public int keresSzamlaszama(String szamlaszam) {
        int i = 0;
        while (i < szamlai.length && !szamlai[i].getSzamlaszam().equals(szamlaszam)) {
            i++;
        }
        if (i < szamlai.length) {
            return i;
        } else {
            return -1;
        }
    }

    public String getNev() {
        return nev;
    }

    public Ugyfel(String nev) {
        this.nev = nev;
        szamlai = new Szamla[0];
    }

    public void FelveszSzamla(Szamla sz) {
        szamlai = java.util.Arrays.copyOf(szamlai,  szamlai.length + 1);
        szamlai[szamlai.length - 1] = sz;
    }

//    public void FelveszFolyoszamla(String szamlaszam) throws BankszamlaNemJoException {
//        Folyoszamla f = new Folyoszamla(szamlaszam);
//        FelveszSzamla(f);
//    }
//
//    public void FelveszBetetiszamla(String szamlaszam, int osszeg, int betetiNapokSzama) throws BankszamlaNemJoException {
//        Betetiszamla b = new Betetiszamla(szamlaszam, osszeg, betetiNapokSzama);
//        FelveszSzamla(b);
//    }
//
//    public void FelveszBetetiszamla(String szamlaszam, int osszeg, Date betetIdopontja, int betetiNapokSzama) throws BankszamlaNemJoException {
//        Betetiszamla b = new Betetiszamla(szamlaszam, osszeg, betetIdopontja, betetiNapokSzama);
//        FelveszSzamla(b);
//    }

    public void szamlaraBetesz(String szamlaszam, int osszeg) throws BankszamlaNemTalalhatoException, NemLehetNegativ, NemLehetASzamlaraBetenniException {
        int sorszam;
        if ((sorszam = keresSzamlaszama(szamlaszam)) == -1) {
            throw new BankszamlaNemTalalhatoException();
        }
        if (!(szamlai[sorszam] instanceof Folyoszamla)){
            throw new NemLehetASzamlaraBetenniException();
        }
        ((Folyoszamla)szamlai[sorszam]).betesz(osszeg);
    }

    @Override
    public String toString() {
        String eredmeny = "\t" + nev + "\n";
        for (int i = 0; i < szamlai.length; i++) {
            eredmeny += "\t\t" + szamlai[i] + "\n";
        }
        return "Ugyfel{" + "nev=" + nev + ", szamlai=" + szamlai + '}';
    }

    void atutal(String forrasSzamlaszam, Szamla celSzamla, int osszeg) throws NincsFedezet, NemLehetNegativ {
        Szamla forrasSzamla = keresSzamlaSzamlaszamAlapjan(forrasSzamlaszam);
        if (forrasSzamla != null && forrasSzamla instanceof Folyoszamla && 
                forrasSzamla.getOsszeg() >= osszeg && celSzamla != null && 
                celSzamla instanceof Folyoszamla){
            ((Folyoszamla)forrasSzamla).kivesz(osszeg);
            ((Folyoszamla)celSzamla).betesz(osszeg);
            
        }
    }
}
