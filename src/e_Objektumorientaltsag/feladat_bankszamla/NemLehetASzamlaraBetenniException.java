/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e_Objektumorientaltsag.feladat_bankszamla;

/**
 *
 * @author nbalazs
 */
class NemLehetASzamlaraBetenniException extends Exception {

    public NemLehetASzamlaraBetenniException() {
        super("Nem lehet a számlára betenni!");
    }
    
}
