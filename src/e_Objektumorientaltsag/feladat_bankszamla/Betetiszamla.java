package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemJoException;
import java.util.Date;

public class Betetiszamla extends Szamla {
    
    private Date betetIdopontja;
    private int betetiNapokSzama;
    private static double kamat = 0.02;

    public Betetiszamla(String haromNyolcasBlock, int osszeg, int betetiNapokSzama) throws BankszamlaNemJoException {
        super(haromNyolcasBlock, osszeg);
        this.betetIdopontja = new Date();
        this.betetiNapokSzama = betetiNapokSzama;
    }

    public Betetiszamla(String haromNyolcasBlock, int osszeg, Date betetIdopontja, int betetiNapokSzama) throws BankszamlaNemJoException {
        super(haromNyolcasBlock, osszeg);
        this.betetIdopontja = betetIdopontja;
        this.betetiNapokSzama = betetiNapokSzama;
    }


    @Override
    public String toString() {
        return "Betétiszámla " + super.toString();
    }
}
