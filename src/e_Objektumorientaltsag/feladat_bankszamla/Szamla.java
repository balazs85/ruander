package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemJoException;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NemLehetNegativ;

public class Szamla {

    protected String szamlaszam;
    protected int osszeg;
    protected static int[] ellenorzoTomb = {9, 7, 3, 1, 9, 7, 3, 1};

    public String getSzamlaszam() {
        return szamlaszam;
    }

    public int getOsszeg() {
        return osszeg;
    }

    public Szamla(String haromNyolcasBlock, int osszeg) throws BankszamlaNemJoException {
        if (!jo_eABankszamla(haromNyolcasBlock)) {
            throw new BankszamlaNemJoException();
        }
        this.szamlaszam = haromNyolcasBlock;
        this.osszeg = osszeg;
    }

    public Szamla(String haromNyolcasBlock) throws BankszamlaNemJoException {
        if (!jo_eABankszamla(haromNyolcasBlock)) {
            throw new BankszamlaNemJoException();
        }
        this.szamlaszam = haromNyolcasBlock;
        this.osszeg = 0;
    }

    private static boolean jo(String nyolcas) {
        int o = 0;
        for (int i = 0; i < 8; i++) {
            o += (nyolcas.charAt(i) - 48) * ellenorzoTomb[i];  //ASCII-ban a számok 48-tól kezdődnek
        }
        return o % 10 == 0;
    }

    public static boolean jo_eABankszamla(String haromNyolcasBlock) {
        String darabok[] = haromNyolcasBlock.split("-");
        int i = 0;
        while (i < darabok.length && jo(darabok[i])) {
            i++;
        }
        return i == darabok.length;
    }

    @Override
    public String toString() {
        return "Számlaszám: " + szamlaszam + ", számlán található összeg:" + osszeg;
    }

    
    
}
