package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemJoException;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemTalalhatoException;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NemLehetNegativ;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NincsIlyenUgyfelException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Fo {
    public static void main(String[] args) {
        try {
            Bank b = new Bank();
            Ugyfel u = new Ugyfel("RUANDER");
            u.FelveszSzamla(new Betetiszamla("11704007-20254548", 100000, 90));
            u.FelveszSzamla(new Folyoszamla("12001008-01453621-01300004"));
            b.ugyfeletHozzaad(u);
            
            
            Ugyfel u2 = new Ugyfel("Apeh");
            b.ugyfeletHozzaad(u2);
            u2.FelveszSzamla(new Folyoszamla("10032000-01076019"));
            u2.szamlaraBetesz("10032000-01076019", 20000);
            //Átutalás
            Ugyfel celUgyfel = b.keresUgyfelSzamlaszamAlapjan("11704007-20254548");
            if (celUgyfel == null){
                System.out.println("Nincs Ilyen ügyfél");
            }
            //itt azért még lenne mit írni
            u2.atutal("10032000-01076019", b.keresSzamlaSzamlaszamAlapjan("12001008-01453621-01300004") , 2000);
        } catch (BankszamlaNemJoException | BankszamlaNemTalalhatoException | 
                NemLehetNegativ | NemLehetASzamlaraBetenniException | NincsFedezet ex) {
            System.out.println(ex.getMessage());
        } 
    }
}
