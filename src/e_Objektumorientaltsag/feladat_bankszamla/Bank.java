package e_Objektumorientaltsag.feladat_bankszamla;

import e_Objektumorientaltsag.feladat_bankszamla.exceptions.BankszamlaNemTalalhatoException;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NemLehetNegativ;
import e_Objektumorientaltsag.feladat_bankszamla.exceptions.NincsIlyenUgyfelException;


public class Bank {
    private Ugyfel[] ugyfelek;

    public Bank() {
        this.ugyfelek = new Ugyfel[0];
    }
    
    public void ugyfeletHozzaad(Ugyfel u){
        ugyfelek = java.util.Arrays.copyOf(ugyfelek, ugyfelek.length+1);
        ugyfelek[ugyfelek.length-1] = u;
    }

    @Override
    public String toString() {
        String eredmeny = "A bank Ügyfelei:\n";
        for (int i = 0; i < ugyfelek.length; i++) {
            eredmeny += ugyfelek[i]+"\n";
        }
        return eredmeny;
    }

    Ugyfel keresUgyfelSzamlaszamAlapjan(String szamlaszam) {
        int i = 0;
        while (i<ugyfelek.length && !ugyfelek[i].van_eSzamlaszama(szamlaszam)){
            i++;
        }
        if (i==ugyfelek.length){
            return null;
        }
        return ugyfelek[i];
    }
    
    Ugyfel keresUgyfelNevAlapjan(String nev) {
        int i = 0;
        while (i<ugyfelek.length && !ugyfelek[i].getNev().equals(nev)){
            i++;
        }
        if (i==ugyfelek.length){
            return null;
        }
        return ugyfelek[i];
    }
    
    Szamla keresSzamlaSzamlaszamAlapjan(String szamlaszam){
        Ugyfel u = keresUgyfelSzamlaszamAlapjan(szamlaszam);
        return u.keresSzamlaSzamlaszamAlapjan(szamlaszam);
    }

    void szamlaraBetesz(String szamlaszam, int osszeg) throws NincsIlyenUgyfelException, BankszamlaNemTalalhatoException, NemLehetNegativ, NemLehetASzamlaraBetenniException {
        Ugyfel u = keresUgyfelSzamlaszamAlapjan(szamlaszam);
        if (u==null){
            throw new NincsIlyenUgyfelException();
        }
        u.szamlaraBetesz(szamlaszam, osszeg);
    }
    
    

}
