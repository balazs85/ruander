package n_Adatbazis.G_Dolgozo;

public class Dolgozo {
    private Integer azon;
    private String nev;
    private Integer belepesEve;
    private Integer kilepesEve;
    private Integer fizetes;

    public Dolgozo(Integer azon, String nev, Integer belepesEve, 
            Integer kilepesEve, Integer fizetes) {
        this.azon = azon;
        this.nev = nev;
        this.belepesEve = belepesEve;
        this.kilepesEve = kilepesEve;
        this.fizetes = fizetes;
    }

    Dolgozo() {
        this.azon = -1;
        this.nev = "";
        this.belepesEve = -1;
        this.kilepesEve = null;
        this.fizetes = -1;    }

    @Override
    public String toString() {
        return nev;
    }

    public boolean equals(Dolgozo obj) {
        return obj.azon.equals(azon) &&
               obj.nev.equals(nev) &&
               obj.belepesEve.equals(belepesEve) &&
               obj.kilepesEve.equals(kilepesEve) &&
               obj.fizetes.equals(fizetes);      
    }

    public Integer getAzon() {
        return azon;
    }

    public void setAzon(Integer azon) {
        this.azon = azon;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public Integer getBelepesEve() {
        return belepesEve;
    }

    public void setBelepesEve(Integer belepesEve) {
        this.belepesEve = belepesEve;
    }

    public Integer getKilepesEve() {
        return kilepesEve;
    }

    public void setKilepesEve(Integer kilepesEve) {
        this.kilepesEve = kilepesEve;
    }

    public Integer getFizetes() {
        return fizetes;
    }

    public void setFizetes(Integer fizetes) {
        this.fizetes = fizetes;
    }
    
    
}
