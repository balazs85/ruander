package n_Adatbazis;

import java.sql.*;

public class D_TablaLekerese {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        String sql = "SELECT * FROM TDolgozo";

        tablabaListazasa(dbFajl, sql);
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);
        
        sql = "SELECT nev, fizetes FROM TDolgozo";
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);

        sql = "SELECT nev, fizetes FROM TDolgozo " 
            + "WHERE kilepesEve IS NULL";
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);

        sql = "SELECT nev, fizetes FROM TDolgozo " 
            + "WHERE kilepesEve IS NULL "
            + "AND belepesEve <= 2000";
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);

        sql = "SELECT nev, fizetes FROM TDolgozo " 
            + "WHERE kilepesEve IS NULL "
            + "AND belepesEve <= 2000 "
            + "ORDER BY nev";
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);

        sql = "SELECT nev, fizetes FROM TDolgozo "
            + "ORDER BY fizetes DESC "
            + "LIMIT 3 ";    
        System.out.println("");
        tablabaListazasa2(dbFajl, sql);

    }

    private static void tablabaListazasa(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                String name = result.getString("nev");
                int belepesEve = result.getInt("belepesEve");
                int kilepesEve = result.getInt("kilepesEve");
                int fizetes = result.getInt("fizetes");

                System.out.println(name + " - " + belepesEve
                        + " - " + kilepesEve + " - " + fizetes);
            }

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private static void tablabaListazasa2(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            ResultSetMetaData rsmd = result.getMetaData();
            int oszlopokSzama = rsmd.getColumnCount();

            while (result.next()) {
                for (int i = 1; i <= oszlopokSzama - 1; i++) {
                    System.out.print(result.getObject(i) + " - ");
                }
                System.out.println(result.getObject(oszlopokSzama));
            }

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
