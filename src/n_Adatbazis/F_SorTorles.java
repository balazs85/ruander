package n_Adatbazis;

import java.sql.*;
import java.util.Calendar;

public class F_SorTorles {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        String sql = "DELETE FROM TDolgozo " +
                "WHERE azon=3";
        torol(dbFajl, sql);
    }

    private static void torol(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            System.out.println("A sort töröltük");

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }


}
