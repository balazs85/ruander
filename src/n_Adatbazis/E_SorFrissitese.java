package n_Adatbazis;

import java.sql.*;
import java.util.Calendar;

public class E_SorFrissitese {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        String sql = "UPDATE TDolgozo " +
                "SET kilepesEve=2014 "+
                "WHERE azon=5";
        modositas(dbFajl, sql);
        
        int ev = Calendar.getInstance().get(Calendar.YEAR);
        sql = "UPDATE TDolgozo " +
                "SET kilepesEve="+ev+" "+
                "WHERE azon=5";
        modositas(dbFajl, sql);
        
        sql = "UPDATE TDolgozo " +
                "SET kilepesEve=? "+
                "WHERE azon=?";
        modositas(dbFajl, sql, ev, 5);
    }

    private static void modositas(String dbFajl, String sql, int ev, int azon) {
        Connection c = null;
        PreparedStatement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.prepareStatement(sql);
            stmt.setInt(1, ev);
            stmt.setInt(2, azon);
            int modositottDb = stmt.executeUpdate();
            System.out.println("A sort módosítottuk");

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private static void modositas(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            System.out.println("A sort módosítottuk");

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }


}
