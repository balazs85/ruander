package n_Adatbazis;

import java.sql.*;

public class A_KapcsolodasAdatbazishoz {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        //Kapcsolódás a szerverhez
        Connection c = kapcsolodas(dbFajl);
        if (c != null) {
            System.out.println("Kapcsolódtunk az adatbázishoz");
            try {
                c.close();
            } catch (SQLException ex) {
            }
        }
    }

    private static Connection kapcsolodas(String dbFajl) {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return c;
    }
}
