package n_Adatbazis;

import java.sql.*;

public class B_TablaElkeszitese {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        String sql = "CREATE TABLE TDolgozo ("
                + "azon INTEGER PRIMARY KEY, "
                + "nev TEXT NOT NULL, "
                + "belepesEve INTEGER NOT NULL, "
                + "kilepesEve INTEGER, "
                + "fizetes INTEGER NOT NULL)";
        tablaKeszites(dbFajl, sql);
    }

    private static void tablaKeszites(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            System.out.println("A táblát hozzáadtuk");

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
