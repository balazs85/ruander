package n_Adatbazis;

import java.sql.*;

public class C_TablaFeltoltese {

    public static void main(String[] args) {
        String dbFajl = "res/sqlite/test.db";
        
        String sql = "INSERT INTO TDolgozo VALUES "
                + "(1,'Senki Alfonz', 1990, 2000, 100000)";        
        tablabaFelvisz(dbFajl, sql);

        sql = "INSERT INTO TDolgozo VALUES "
                + "(2, 'Szuper Áron', 2000, NULL, 120000)";        
        tablabaFelvisz(dbFajl, sql);

        sql = "INSERT INTO TDolgozo (nev, belepesEve, fizetes) "
                + "VALUES ('Szalmon Ella', 1998, 120000)";        
        tablabaFelvisz(dbFajl, sql);

        sql = "INSERT INTO TDolgozo (nev, belepesEve, kilepesEve, fizetes) "
                + "VALUES ('T Eszter', 1995, 2005, 110000)";        
        tablabaFelvisz(dbFajl, sql);

        sql = "INSERT INTO TDolgozo (nev, belepesEve, fizetes) "
                + "VALUES ('Kemény Szilárd', 2005, 110000)";        
        tablabaFelvisz(dbFajl, sql);
    }

    private static void tablabaFelvisz(String dbFajl, String sql) {
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtunk az adatbázishoz");

            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
            System.out.println("A sort hozzáadtuk");

            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
