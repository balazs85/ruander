/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package n_Adatbazis;

import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author nbalazs
 */
public class G_TablakKapcsolata {
    public static void main(String[] args) {
        try {
            ArrayList<String> parancsok = parancsBeolvas("res/sqlite/Parancsok.sql");
            //System.out.println(parancsok);
            String dbFajl = "res/sqlite/diakokJegyek.db";
            Connection c = kapcsolodas(dbFajl);
            parancsokatVegrehajt(parancsok, c);
            
            c.close();
        } catch (SQLException sQLException) {
        }
    }
    
    private static void parancsokatVegrehajt(ArrayList<String> parancsok, Connection c) {
        for (String parancs : parancsok) {
            vegrehajt(parancs, c);
            //nem lesz jó, egy connection kellene
        }
    }
    
    private static void vegrehajt(String sql, Connection c) {  
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    private static Connection kapcsolodas(String dbFajl) {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return c;
    }

    
    private static ArrayList<String> parancsBeolvas(String fajlNev) {
        ArrayList<String> parancsok = new ArrayList<>();
        try {
            Scanner s = new Scanner(new BufferedReader(new FileReader(fajlNev)));
            s.useDelimiter(";");            
            while (s.hasNext()) {                
                parancsok.add(s.next());
                
            }
        } catch (FileNotFoundException fileNotFoundException) {
        }
        
        return parancsok;
    }

}
