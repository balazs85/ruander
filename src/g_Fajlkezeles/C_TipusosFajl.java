/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g_Fajlkezeles;

import java.io.*;
/**
 *
 * @author nbalazs
 */
public class C_TipusosFajl {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String[] nevek = {"Kovács István", "Kiss Lajos", "Horváth Lujza"};
        int[] magassagok = {168, 195, 164};
        
        //Kiírás típusos fájlba
        DataOutputStream out = new DataOutputStream(
            new BufferedOutputStream(
            new FileOutputStream("diakok.dat")));
        for (int i = 0; i < nevek.length; i++) {
            out.writeUTF(nevek[i]);
            out.writeInt(magassagok[i]);
        }
        out.close();
        
        //Beolvasás típusos fájlból
        DataInputStream in = new DataInputStream(
            new BufferedInputStream(
            new FileInputStream("diakok.dat")));

        String nev;
        int magassag;
        try {
            while (true) {
                nev = in.readUTF();
                magassag = in.readInt();
                System.out.println(nev +" "+magassag );
            }
        } catch (EOFException e) {
            System.out.println("Vége a fájlnak");
        }
        
    }
}
