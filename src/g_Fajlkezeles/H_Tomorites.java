package g_Fajlkezeles;

import java.io.*;
import java.util.jar.JarEntry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.*;

public class H_Tomorites {

    public static void main(String[] args) {
        byte[] buffer = new byte[1024];

        try {
            FileOutputStream os = new FileOutputStream("tomor.zip");
            ZipOutputStream zos = new ZipOutputStream(os);
            ZipEntry ze = new ZipEntry("Koltsegvetes.xls");
            zos.putNextEntry(ze);

            FileInputStream in = new FileInputStream("Koltsegvetes.xls");
            int hossz;
            while ((hossz = in.read(buffer)) > 0) {
                zos.write(buffer, 0, hossz);
            }
            in.close();
            zos.closeEntry();
            zos.close();
            System.out.println("Kész");

        } catch (FileNotFoundException fileNotFoundException) {
        } catch (IOException ex) {
        }
    }
}
