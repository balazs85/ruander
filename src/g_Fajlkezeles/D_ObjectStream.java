package g_Fajlkezeles;

import java.io.*;
import java.util.Scanner;

public class D_ObjectStream {
    private static class Diak implements Serializable{
        private String nev;
        private int magassag;
        
        public Diak(String nev, int magassag) {
            this.nev = nev;
            this.magassag = magassag;
        }

        @Override
        public String toString() {
            return nev + " (" + magassag + ')';
        }  
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Diak[] diakok = DiakBeolvasFajlbol("diakok.txt");
        FajlbaMent("diakokObject.dat", diakok);
        Diak[] diakok2 = FajlbolOlvas("diakokObject.dat");
        for (int i = 0; i < diakok2.length; i++) {
            System.out.println(diakok2[i]);
        }
    }

    private static Diak[] FajlbolOlvas(String fileNev) throws IOException {
        Diak[] osztaly = new Diak[1000];
        ObjectInputStream in = new ObjectInputStream(
            new FileInputStream(fileNev));
        int i = 0;
        try {
            while (true) {
                osztaly[i] = (Diak)in.readObject();
                i++;
            }
        } catch (EOFException e) {
            return java.util.Arrays.copyOf(osztaly, i);
        } catch (ClassNotFoundException ex) {
        }
        return null;
    }

    private static void FajlbaMent(String fileNev, Diak[] diakok) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(
            new FileOutputStream(fileNev));
        for (int i = 0; i < diakok.length; i++) {
            out.writeObject(diakok[i]);
        }
        out.close();
    }
    
    private static Diak[] DiakBeolvasFajlbol(String fajlNev) throws FileNotFoundException {
        Diak[] osztaly = new Diak[1000];
        int i = 0;
        Scanner s = new Scanner(new BufferedReader(new FileReader(fajlNev)));
        s.useDelimiter(",|\\n");  
        while (s.hasNext()) {        
            osztaly[i] = new Diak(s.next(), s.nextInt());
            i++;
        }
        return java.util.Arrays.copyOf(osztaly, i);
    }

}
