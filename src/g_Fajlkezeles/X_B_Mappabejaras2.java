/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g_Fajlkezeles;

import java.io.*;
import java.nio.file.*;

/**
 *
 * @author nbalazs
 */
public class X_B_Mappabejaras2 {
    public static void main(String[] args) throws IOException {
        String utvonalSzoveg = new File(".").getCanonicalPath();
        Path p = Paths.get(utvonalSzoveg);
        int szint = 0;
        
        bejar(p, szint);        
    }

    private static void bejar(Path p, int szint) {
        try {
            DirectoryStream<Path> stream = Files.newDirectoryStream(p); 
            for (Path file: stream) {
                if (Files.isDirectory(file)){
                    for (int i = 0; i < szint; i++) {
                        System.out.print("\t");
                    }
                    System.out.println(file.getFileName());
                    bejar(file, szint + 1);
                }
            }
        } catch (IOException | DirectoryIteratorException x) {
            System.err.println(x);
        }        
    }
}
