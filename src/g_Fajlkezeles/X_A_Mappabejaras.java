/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g_Fajlkezeles;

import java.io.*;
import java.nio.file.*;

/**
 *
 * @author nbalazs
 */
public class X_A_Mappabejaras {
    public static void main(String[] args) throws IOException {
        String utvonalSzoveg = new File(".").getCanonicalPath();
        Path p = Paths.get(utvonalSzoveg);
        
        bejar(p);        
    }

private static void bejar(Path p) {
    try {
        DirectoryStream<Path> stream = Files.newDirectoryStream(p); 
        for (Path file: stream) {
            if (Files.isDirectory(file)){
                System.out.println(file);
                bejar(file);
            }
        }
    } catch (IOException | DirectoryIteratorException x) {
        System.err.println(x);
    }        
}
}
