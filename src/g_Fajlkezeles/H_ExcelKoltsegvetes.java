package g_Fajlkezeles;

import java.io.*;
import java.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

public class H_ExcelKoltsegvetes {

    private static class Koltseg {

        Date rogzites;
        String megnevezes;
        int ertek;

        public Koltseg(Date rogzites, String megnevezes, int ertek) {
            this.rogzites = rogzites;
            this.megnevezes = megnevezes;
            this.ertek = ertek;
        }

        public Date getRogzites() {
            return rogzites;
        }
        
        public String getRogzitesString(){
            return String.format("%tY. %tB %te.", rogzites, rogzites, rogzites);
        }

        public String getMegnevezes() {
            return megnevezes;
        }

        public int getErtek() {
            return ertek;
        }

        @Override
        public String toString() {
            return getRogzitesString() + "\t" + megnevezes + "\t" + ertek;
        }
    }

    private static class Koltsegek {

        Koltseg[] koltsegekTomb;

        public Koltsegek(String fajlnev) throws FileNotFoundException, IOException {
            koltsegekTomb = new Koltseg[1000];
            FileInputStream file = new FileInputStream(new File("Koltsegvetes.xls"));
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            Row row;
            //az első 4 sorban nincs nekünk kellő dolog
            for (int i = 0; i < 4; i++) {
                row = rowIterator.next();
            }
            int i = 0;
            while (rowIterator.hasNext()) {
                row = rowIterator.next();
                Date datum = row.getCell(0).getDateCellValue();
                String megnevezes = row.getCell(1).getStringCellValue();
                int osszeg = (int) row.getCell(2).getNumericCellValue();
                koltsegekTomb[i] = new Koltseg(datum, megnevezes, osszeg);
                i++;
            }
            koltsegekTomb = java.util.Arrays.copyOf(koltsegekTomb, i);
        }

        @Override
        public String toString() {
            String result = "";
            for (int i = 0; i < koltsegekTomb.length; i++) {
                result += koltsegekTomb[i] + "\n";
            }
            return result;
        }

    }

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Koltsegek k = new Koltsegek("Koltsegvetes.xls");
        System.out.println(k);
    }

}
