package g_Fajlkezeles;

import java.io.*;
import java.util.Scanner;

public class A_Scanner {
    public static void main(String[] args) {
        Scanner s = null;
        try {
            s = new Scanner(new BufferedReader(new FileReader("elso.txt")));
            
            while (s.hasNext()) {
                System.out.println(s.next());
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Nincs ilyen fájl");
        } finally {
            if (s != null) {
                s.close();
            }
        }
    }
}
