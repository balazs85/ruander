package g_Fajlkezeles;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class B_ScannerDiakokBeolvasasa {
  private static class Diak {
        private String nev;
        private int magassag;
        
        public Diak(String nev, int magassag) {
            this.nev = nev;
            this.magassag = magassag;
        }

        @Override
        public String toString() {
            return nev + " (" + magassag + ')';
        }  
    }

    public static void main(String[] args) {
        try {
            Diak[] diakok = DiakBeolvasFajlbol("diakok.txt");
            for (int i = 0; i < diakok.length; i++) {
                System.out.println(diakok[i]);
            }
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println("A fájl nem létezik");
        }
    }

    private static Diak[] DiakBeolvasFajlbol(String fajlNev) throws FileNotFoundException {
        Diak[] osztaly = new Diak[1000];
        int i = 0;
        Scanner s = new Scanner(new BufferedReader(new FileReader(fajlNev)));
        s.useDelimiter(",|\\n");  
        while (s.hasNext()) {        
            osztaly[i] = new Diak(s.next(), s.nextInt());
            i++;
        }
        return java.util.Arrays.copyOf(osztaly, i);
    }
  
}
