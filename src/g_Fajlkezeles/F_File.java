package g_Fajlkezeles;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.*;

public class F_File {    
    public static void main(String[] args) throws IOException{
        //Útvonal
        String path = new File(".").getCanonicalPath();
        Path p = Paths.get(path);
        
        //Fájl
        Path p2 = Paths.get(path+"/diakok.txt");
        System.out.println(Files.isDirectory(p));
        System.out.println(Files.isDirectory(p2));
        System.out.println(Files.isRegularFile(p));
        System.out.println(Files.isRegularFile(p2));
        System.out.println(Files.isReadable(p));
        System.out.println(Files.isReadable(p2));
        System.out.println(Files.isExecutable(p));
        System.out.println(Files.isExecutable(p2));
        
        //mappa létrehozás
        //Path mu = Paths.get("tesztMappa");
        //Files.createDirectory(mu);
        //Files.createDirectories(Paths.get("alma/korte/barack"));
        
        //mappák listázása
        try {
            DirectoryStream<Path> stream = Files.newDirectoryStream(p); 
            for (Path file: stream) {
                System.out.println(file.getFileName());
            }
        } catch (IOException | DirectoryIteratorException x) {
            System.err.println(x);
        }
        
        //másolás
        Path forras = Paths.get("diakok.txt");
        Path cel = Paths.get("tesztMappa/diakok.txt");
        Files.copy(forras,cel,COPY_ATTRIBUTES);
    }
}
