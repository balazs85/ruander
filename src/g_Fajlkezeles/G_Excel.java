package g_Fajlkezeles;

import java.io.*;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

public class G_Excel {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        FileInputStream file = new FileInputStream(new File("Koltsegvetes.xls"));
	HSSFWorkbook workbook = new HSSFWorkbook(file);	
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        while(rowIterator.hasNext()) {
	        Row row = rowIterator.next();
	        Iterator<Cell> cellIterator = row.cellIterator();
	        while(cellIterator.hasNext()) {
	            Cell cell = cellIterator.next();
	            switch(cell.getCellType()) {
	                case Cell.CELL_TYPE_BOOLEAN:
	                    System.out.print(cell.getBooleanCellValue() + "\t\t");
	                    break;
	                case Cell.CELL_TYPE_NUMERIC:
	                    System.out.print(cell.getNumericCellValue() + "\t\t");
	                    break;
	                case Cell.CELL_TYPE_STRING:
	                    System.out.print(cell.getStringCellValue() + "\t\t");
	                    break;
	            }
	        }
	        System.out.println("");

        }
    }

    private static void menjunkVegigAFajlon(String fajlnev) throws FileNotFoundException, IOException {
    }
}
