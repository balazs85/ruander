package g_Fajlkezeles;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class E_Path {    
    public static void main(String[] args) throws IOException{
        //Útvonal
        String path = new File(".").getCanonicalPath();
        Path p = Paths.get(path);
        System.out.println(p.toString());
        System.out.println(p.getFileName());
        System.out.println(p.getName(0));
        System.out.println(p.getNameCount());
        System.out.println(p.subpath(1,3));
        System.out.println(p.getParent());
        System.out.println(p.getRoot());
    }
}
