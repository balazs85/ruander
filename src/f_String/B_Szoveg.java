package f_String;

public class B_Szoveg {

    public static void main(String[] args) {
        double d = 1234.56;
        String s = Double.toString(d);

        int pontHelye = s.indexOf('.');
        int egeszSzamjegyek = pontHelye;
        int tortSzamjegyek = s.length() - pontHelye - 1;
        System.out.println(egeszSzamjegyek);
        System.out.println(tortSzamjegyek);
        
        String szemetes = "  helyköz   ";
        String tiszta = szemetes.trim();
        System.out.println(tiszta);
        
        String kotojel = "alma-korte-barack";
        String alulvon = kotojel.replace('-', '_');
        System.out.println(alulvon);
        
        System.out.println(("ab").compareTo("ba"));     
    }
}
