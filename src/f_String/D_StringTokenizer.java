package f_String;

import java.util.StringTokenizer;

public class D_StringTokenizer {

    public static void main(String[] args) {
String sz = "Sokan azt hiszik, gondolkodnak, ";
sz += "pedig csak újrarendezik előítéleteiket.";

System.out.println("Szóközzel elválasztva:");
StringTokenizer st = new StringTokenizer(sz);
while(st.hasMoreElements()){
    System.out.println(st.nextElement());
}
System.out.println("");

System.out.println("Vesszővel elválasztva:");
StringTokenizer st2 = new StringTokenizer(sz, ",");
while(st2.hasMoreElements()){
    System.out.println(st2.nextElement());
}
    }
}
