package f_String;

public class A_Szamok {
    public static void main(String[] args) {
        int a, b;
        Integer aa, bb;
        Integer aaa, bbb;
        
        a = 2; b = 2;
        
        aa = 2; bb = 2;
        
        aaa = new Integer(2);
        bbb = new Integer(2);
   
        System.out.println(a == b);
        System.out.println(aa == bb);
        System.out.println(aaa == bbb);
        
        int c = Integer.parseInt("10");
        Integer cc = Integer.valueOf("10");
        int d = Integer.valueOf("10");
        Integer dd = Integer.parseInt("10");
        
        System.out.println(c + cc + d + dd);
    }
}
