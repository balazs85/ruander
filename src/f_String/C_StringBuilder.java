package f_String;

public class C_StringBuilder {

    public static void main(String[] args) {
        String a = "Hello";
        a = a + " world";
        System.out.println(a);
        
        StringBuilder sb = new StringBuilder();
        sb.append("süt");
        sb.append(" a nap");
        System.out.println(sb);
        sb.insert(0, "szépen ");
        System.out.println(sb);
        sb.delete(4, 10);
        System.out.println(sb);
        
        
    }
}
