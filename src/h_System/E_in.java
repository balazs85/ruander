package h_System;

import java.io.IOException;

public class E_in {
    public static void main(String[] args) {
        //beolvasKarakter();
        beolvasEnterig();
    }

    private static void beolvasKarakter() {
        int inChar;
        System.out.println("Írj be egy karaktert!");
        try {
            inChar = System.in.read();
            System.out.print("Amit beütöttél: ");
            System.out.println(inChar);
        }
        catch (IOException e){
            System.out.println("Olvasási hiba");
        }
    }
    
    private static void beolvasEnterig() {
        int inChar = 0;
        String sz="";
        System.out.println("Írj be egy karaktert!");
        try {
            while(inChar !=(int)('\n')){
                inChar = System.in.read();
                sz += (char)inChar;
            }
            System.out.println("Amit beírtál:"+sz);
        }
        catch (IOException e){
            System.out.println("Olvasási hiba");
        }
    }
}
