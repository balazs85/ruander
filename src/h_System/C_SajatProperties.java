package h_System;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class C_SajatProperties {
    public static void main(String[] args) {
        
        mentes();
        beolvasas();
        
 
    }

    private static void mentes() {
        Properties prop = new Properties();
        OutputStream output = null;
        try {
            output = new FileOutputStream("config.properties");
            prop.setProperty("database", "localhost");
            prop.setProperty("dbuser", "nbalazs");
            prop.setProperty("dbpassword", "password");
            prop.store(output, null);
        } catch (IOException io) {
	} finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                }
            }
	}
    }

    private static void beolvasas() {
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            System.out.println(prop.getProperty("database"));
            System.out.println(prop.getProperty("dbuser"));
            System.out.println(prop.getProperty("dbpassword"));
            
            
            
        } catch (IOException ex) {
	} finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
	}    
    }
}
