package h_System;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class B_Properties {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Properties p = System.getProperties();
        System.out.println(p.getProperty("user.name"));
        System.out.println(p.getProperty("file.separator"));
        System.out.println(p.getProperty("os.name"));
        System.out.println(p.getProperty("java.runtime.version"));

        p.list(System.out);
        
        
//        // create and load default properties
//        Properties defaultProps = new Properties();
//        FileInputStream in = new FileInputStream("defaultProperties");
//        defaultProps.load(in);
//        in.close();
//
//        // create application properties with default
//        Properties applicationProps = new Properties(defaultProps);
//
//        // now load properties 
//        // from last invocation
//        in = new FileInputStream("appProperties");
//        applicationProps.load(in);
//        in.close();
    }
}
