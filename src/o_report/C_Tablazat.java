package o_report;

import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileNotFoundException;

public class C_Tablazat {

    private static Font cimFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font fontosFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);

    private static Font tablazatFejFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
    private static Font tablazatFont = new Font(Font.FontFamily.TIMES_ROMAN, 12);

    public static void main(String[] args) {
        try {
            String fNev = "res/pdf/C_Tablazat2.pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fNev));
            document.open();
            metaAdatok(document);
            tartalom(document);
            document.close();

        } catch (FileNotFoundException fileNotFoundException) {
        } catch (DocumentException documentException) {
        }
    }

    private static void tartalom(Document document) {
        try {
            Paragraph cim = new Paragraph("Ez a cím", cimFont);
            Chapter foCimFejezet = new Chapter(cim, 1);
            Paragraph szoveg = new Paragraph("Ennyit azért olvass el!", fontosFont);
            foCimFejezet.add(szoveg);
            document.add(foCimFejezet);
            document.newPage();

            Paragraph cim2 = new Paragraph("Ez már maga a dokumentum", cimFont);
            cim2.setSpacingAfter(12);
            Chapter tartalomFejezet = new Chapter(cim2, 2);
            PdfPTable tablazat = tablazatElkeszit();
            tartalomFejezet.add(tablazat);

            document.add(tartalomFejezet);

        } catch (DocumentException documentException) {
        }
    }

    private static PdfPTable tablazatElkeszit() {
        String[] fej = new String[]{"Terméknév", "Nettó ár", "Bruttó Ár"};
        Object[][] tartalom
                = new Object[][]{{"Tej", 180},
                {"Kenyér", 200},
                {"Zsömle", 10}};
        PdfPTable tablazat = tablazatElkeszit(fej);
        tartalmatHozzaad(tablazat, tartalom);
        return tablazat;
    }

    private static void tartalmatHozzaad(PdfPTable tablazat, Object[][] tartalom) {
        String nev;
        Double netto;
        Double brutto;
        for (Object[] sor : tartalom) {
            nev = sor[0].toString();
            netto = Double.valueOf(sor[1].toString());
            brutto = netto * 1.27;
            tablazat.addCell(nev);
            tablazat.addCell(netto.toString());
            tablazat.addCell(brutto.toString());
        }
    }

    private static PdfPTable tablazatElkeszit(String[] fej) {
        PdfPTable table = null;
        try {
            table = new PdfPTable(fej.length);
            table.setWidthPercentage(95);
            table.setWidths(new int[]{2, 1, 1});
            table.getDefaultCell().setPadding(4);

            for (String element : fej) {
                PdfPCell cell = new PdfPCell(new Paragraph(element, tablazatFejFont));
                cell.setPadding(3);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }
        } catch (DocumentException documentException) {
        }
        return table;
    }

    private static void metaAdatok(Document document) {
        document.addTitle("Az első PDF dokumentumom");
        document.addSubject("Az iText használata");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Nagy Balázs Tamás");
        document.addCreator("Nagy Balázs Tamás");
    }

}
