package o_report;

import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileNotFoundException;

public class A_HelloPDF {

    public static void main(String[] args) {
        try {
            String fNev = "res/pdf/A_Hello.pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fNev));
            document.open();
            metaAdatok(document);
            tartalom(document);
            document.close();

        } catch (FileNotFoundException fileNotFoundException) {
        } catch (DocumentException documentException) {
        }
    }

    private static void tartalom(Document document) {
        try {
            Paragraph p = new Paragraph("Hello PDF!");
            document.add(p);
        } catch (DocumentException documentException) {
        }
    }

    private static void metaAdatok(Document document) {
        document.addTitle("Az első PDF dokumentumom");
        document.addSubject("Az iText használata");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Nagy Balázs Tamás");
        document.addCreator("Nagy Balázs Tamás");
    }

}
