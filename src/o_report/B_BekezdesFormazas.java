package o_report;

import java.io.FileOutputStream;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileNotFoundException;

public class B_BekezdesFormazas {

    private static Font cimFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    private static Font fontosFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);

    public static void main(String[] args) {
        try {
            String fNev = "res/pdf/B_Szoveg2.pdf";
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(fNev));
            document.open();
            metaAdatok(document);
            tartalom2(document);
            document.close();
            
        } catch (FileNotFoundException fileNotFoundException) {
        } catch (DocumentException documentException) {
        }
    }

    private static void tartalom2(Document document) {
        try {
            Paragraph cim = new Paragraph("Ez a cím", cimFont);
            Chapter foCimFejezet = new Chapter(cim, 1);
            Paragraph szoveg = new Paragraph("Ennyit azért olvass el!", fontosFont);
            foCimFejezet.add(szoveg);
            document.add(foCimFejezet);
            document.newPage();

            Paragraph cim2 = new Paragraph("Ez már maga a dokumentum", cimFont);
            Chapter tartalomFejezet = new Chapter(cim2, 2);

            document.add(tartalomFejezet);
            
        } catch (DocumentException documentException) {
        }
    }

    
    private static void tartalom(Document document) {
        try {
            Paragraph cim = new Paragraph("Ez a cím", cimFont);
            document.add(cim);
            Paragraph szoveg = new Paragraph("Ennyit azért olvass el!", fontosFont);
            document.add(szoveg);
            
        } catch (DocumentException documentException) {
        }
    }
    
    private static void metaAdatok(Document document) {
        document.addTitle("Az első PDF dokumentumom");
        document.addSubject("Az iText használata");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Nagy Balázs Tamás");
        document.addCreator("Nagy Balázs Tamás");
    }

}
