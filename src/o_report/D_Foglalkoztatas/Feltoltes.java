package o_report.D_Foglalkoztatas;

import java.io.*;
import java.sql.*;

public class Feltoltes {

    static String dbFajl = "res/sqlite/foglalkoztatas.db";
    
    public static void main(String[] args) {
        
        //Kapcsolódás a szerverhez
        Connection c = kapcsolodas(dbFajl);
        //Táblák feltöltése
        feltotlSzovegekbol(c);
        
        //c.close();
    }

    
    private static void feltotlSzovegekbol(Connection c) {
        feltolt(c, "regiok", "res/statisztika/regiok.txt");
        feltolt(c, "megyek", "res/statisztika/megyek.txt");
        feltolt(c, "foglalkozasok", "res/statisztika/foglalkozasok.txt");
        feltolt(c, "lakosok", "res/statisztika/lakosok.txt");
    }
    
    private static void feltolt(Connection c, String tabla, String fajlnev) {
        try {

            BufferedReader br = new BufferedReader(new FileReader(fajlnev));
//            File f = new File(fajlnev);
//
//            BufferedReader br = new BufferedReader(
//            new InputStreamReader(new FileInputStream(f), "UTF-8"));
//            
            String sor;
            String sql;

            sor = br.readLine();

            while ((sor = br.readLine()) != null) {
                sql = "INSERT INTO " + tabla + " VALUES (";
                String[] darabok = sor.split("\t");
                for (int i = 0; i < darabok.length; i++) {
                    if (!szam_e(darabok[i])) {
                        sql += "\"" + darabok[i] + "\", ";
                    } else {
                        sql += darabok[i] + ", ";
                    }
                }
                //leveszem az utolsó ", "-t
                sql = sql.substring(0,sql.length()-2) + ")";
                sqltFuttat(c, sql);
            }
            br.close();
        } catch (IOException iOException) {
        }
    }
    
    private static boolean szam_e(String s) {
        try { 
            Integer.parseInt(s); 
        } catch(NumberFormatException e) { 
            return false; 
        }
        return true;
    }

    private static void sqltFuttat(Connection c, String sql) {
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection kapcsolodas(String dbFajl) {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return c;
    }


}
