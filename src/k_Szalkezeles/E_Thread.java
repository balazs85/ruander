package k_Szalkezeles;

class UzenetMegjelenito implements Runnable {

    private String uzenet;

    public UzenetMegjelenito(String uzenet) {
        this.uzenet = uzenet;
    }

    public void run() {
        while (true) {
            System.out.println(uzenet);
        }
    }
}

class GondoltamEgySzamra extends Thread {

    private int szam;

    public GondoltamEgySzamra(int szam) {
        this.szam = szam;
    }

    public void run() {
        int szamlalo = 0;
        int tipp;
        do {
            tipp = (int) (Math.random() * 100 + 1);
            System.out.println(this.getName()
                    + " tipp: " + tipp);
            szamlalo++;
        } while (tipp != szam);
        System.out.println("** Eltaláltad! " + this.getName()
                + " " + szamlalo + " próbálkozásból!!!!!!!!");
    }
}

public class E_Thread {

    public static void main(String[] args) {
        Thread thread1 = new Thread(new UzenetMegjelenito("Hello"));
        thread1.setDaemon(true);
        thread1.setName("hello");
        System.out.println("Hellot köszönő szál(thread1) elindult...");
        thread1.start();

        Thread thread2 = new Thread(new UzenetMegjelenito("Viszlát"));
        thread2.setPriority(Thread.MIN_PRIORITY);
        thread2.setDaemon(true);
        System.out.println("Viszláttal köszönő szál(thread2) elindult...");
        thread2.start();

        System.out.println("Gondoltam a 27-re (thread3) elindult");
        Thread thread3 = new GondoltamEgySzamra(27);
        thread3.setName("thread3");
        thread3.start();
        try { thread3.join();
        } catch (InterruptedException e) {
            System.out.println("Thread megszakadt.");
        }

        System.out.println("Gondoltam a 75-re (thread4) elindult");
        Thread thread4 = new GondoltamEgySzamra(75);
        thread4.setName("thread4");
        thread4.start();
        System.out.println("main() véget ért...");
    }
}
