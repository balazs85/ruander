package k_Szalkezeles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class G_ChatKliens {
    public static void main(String[] args) {
        
        //szerver felé irányuló socket
        Socket clientSocket = null;
        //bemeneti, kimeneti hálózati streamek
        BufferedReader is = null;
        PrintStream os = null;
        //konzolról olvasó stream
        BufferedReader konzolInput = null;
        
        
        try {
            clientSocket = new Socket("localhost", 2222);
            os = new PrintStream(clientSocket.getOutputStream());
            is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            konzolInput = new BufferedReader(new InputStreamReader(System.in));
        } catch (IOException iOException) {
            System.out.println("Nem lehet kapcsolódni a szerverhez");
        }
        
        if (clientSocket != null && os != null && is != null){
            try {
                System.out.println("A kliens elindult, írjon be valamit! Kilépéshez írja be, hogy 'Ok'");
                String responseLine;
                os.println(konzolInput.readLine());       //amit a konzolról beolvasunk, rögtön a szervernek küldjük
                while ((responseLine = is.readLine()) != null) {    //amíg tudunk a szerverről olvasni
                    System.out.println(responseLine);               //kiírjuk, amit a szervertől kapunk
                    if (responseLine.indexOf("Ok") != -1) {         //ha megvan benne az Ok, kilép
                        break;
                    }
                    os.println(konzolInput.readLine());             //amit konzolról beolvasunk elküldjük a szervernek
                }
                
                os.close();
                is.close();
                clientSocket.close();
            } catch (IOException iOException) {}
        }
    }
}
