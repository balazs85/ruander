package k_Szalkezeles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

//probléma: csak egy klienst kezel
public class H_ChatSzerver2 {

            
    //szerver szocketet, amihez lehet csatlakozni
    private static ServerSocket serverSocket = null;
    //socket, amivel elkapja a klienstől jövő adatokat
    private static Socket clientSocket = null;
    
    private static final int maxClientsCount = 10;
    private static final clientThread[] threads = new clientThread[maxClientsCount];


    
    public static void main(String[] args) {
        //bemeneti, kimeneti hálózati streamek
//        BufferedReader is = null;
//        PrintStream os = null;
        
        try {
            serverSocket = new ServerSocket(2222);
        } catch (IOException ex) {
        }
        System.out.println("Szerver elindult, megállítása <ctrl><c> kombinációval");
        
        while (true) {
            try {
                //elkapja a bejövő kapcsolatot
                clientSocket = serverSocket.accept();
                
                int i = 0;
                for (i = 0; i < maxClientsCount; i++) {
                    if (threads[i] == null) {
                        //ha van még szabad szocket, lefoglalja a bejövő kapcsolatnak
                        (threads[i] = new clientThread(clientSocket, threads)).start();
                        break;
                    }
                }
                //nem lépett ki break-kel, azaz nem talált szabad szocketet
                if (i == maxClientsCount) {
                    PrintStream os = new PrintStream(clientSocket.getOutputStream());
                    os.println("Server too busy. Try later.");
                    os.close();
                    clientSocket.close();
                }
            } catch (IOException iOException) {
            }   
        }
    }
}

class clientThread extends Thread {

    //be, ki streamek
    private BufferedReader is = null;
    private PrintStream os = null;
    //saját szocket
    private Socket clientSocket = null;
    //minden szál tud a többi szálról
    private final clientThread[] threads;
    private int maxClientsCount;

    public clientThread(Socket clientSocket, clientThread[] threads) {
        this.clientSocket = clientSocket;
        this.threads = threads;
        maxClientsCount = threads.length;
    }

    public void run() {
        int maxClientsCount = this.maxClientsCount;
        clientThread[] threads = this.threads;

        try {
            //Kapcsolat felépítésénél streamek beállítása
            is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            os = new PrintStream(clientSocket.getOutputStream());
            //Üdvözlő szövegeket küldünk az új felhasználónak
            //szerverhez képest output
            os.println("Add meg a neved!");
            String name = is.readLine().trim();
            os.println("Üdvözöllek " + name
                + " a csetszobánkban.\nHa ki akarsz lépni, írd be, hogy '/quit' egy új sorba!");
            for (int i = 0; i < maxClientsCount; i++) {
                //mindenkinek (kivéve az új tagot) bejelentjük, hogy új tag jött
                if (threads[i] != null && threads[i] != this) {
                    threads[i].os.println("*** " + name
                        + " belépett a szobába !!! ***");
                }
            }
            while (true) {
                //miután belépett a felhasználó, itt fog pörögni,
                String line = is.readLine();
                //amíg csak /quit-t nem üt be
                if (line.startsWith("/quit")) {
                    break;
                }
                //ha valamit üzen, mindenkinek elküldjük ezt
                for (int i = 0; i < maxClientsCount; i++) {
                    if (threads[i] != null) {
                        threads[i].os.println("<" + name + "> " + line);
                    }
                }
            }
            //ha k, mlépett, mindenkinek jelezzük ezt
            for (int i = 0; i < maxClientsCount; i++) {
                if (threads[i] != null && threads[i] != this) {
                    threads[i].os.println("*** " + name
                        + " elhagyta a szobát !!! ***");
                }
            }
            //az elbúcsúzó szöveget küldünk a felhasználónak
            os.println("*** Viszlát " + name + " ***");

            //az aktuális threadet felszabadítjuk
            for (int i = 0; i < maxClientsCount; i++) {
                if (threads[i] == this) {
                    threads[i] = null;
                }
            }

            is.close();
            os.close();
            clientSocket.close();
        } catch (IOException e) {
        }
    }
}
