package k_Szalkezeles;

class Futtathato implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName()
                    + " - " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {}
        }
    }

}

public class D_JoinPelda {
    public static void main(String[] args) {
        //aSzalakKiszamithatatlanok();
        megvalositasJonnal();
        //megvalositasJonnal2();
    }

    private static void aSzalakKiszamithatatlanok() {
        Futtathato fu = new Futtathato();
        Thread t1 = new Thread(fu, "Elso");
        Thread t2 = new Thread(fu, "Masodik");
        t1.start();
        t2.start();
    }

    private static void megvalositasJonnal() {
        Futtathato fu = new Futtathato();
        Thread t1 = new Thread(fu, "Elso");
        Thread t2 = new Thread(fu, "Masodik");
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
            System.out.println("Vége");
        } catch (InterruptedException interruptedException) {}
    }

    private static void megvalositasJonnal2() {
        Futtathato fu = new Futtathato();
        Thread t1 = new Thread(fu, "Elso");
        Thread t2 = new Thread(fu, "Masodik");
        try {
            t1.start();
            t1.join();
            t2.start();
            t2.join();
            System.out.println("Vége");
        } catch (InterruptedException interruptedException) {}
    }
}
