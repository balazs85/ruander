package k_Szalkezeles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

//probléma: csak egy klienst kezel
public class G_ChatSzerver {
    public static void main(String[] args) {
        
        //szerver szocketet, amihez lehet csatlakozni
        ServerSocket echoServer = null;
        //socket, amivel elkapja a klienstől jövő adatokat
        Socket clientSocket = null;
        //bemeneti, kimeneti hálózati streamek
        BufferedReader is = null;
        PrintStream os = null;
        
        try {
            echoServer = new ServerSocket(2222);
        } catch (IOException ex) {
        }
        System.out.println("Szerver elindult, megállítása <ctrl><c> kombinációval");
        
        try {
            //elkapja a bejövő kapcsolatot
            clientSocket = echoServer.accept();
            //klienssel kapcsolatot tartó streamek
            os = new PrintStream(clientSocket.getOutputStream());
            is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            
            while (true) {      //amit kap,  visszaküldi átalakítva          
                String line = is.readLine();
                os.println("From server: "+line);
            }
        } catch (IOException iOException) {
        }        
    }
}
