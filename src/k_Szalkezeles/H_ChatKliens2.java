package k_Szalkezeles;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H_ChatKliens2 implements Runnable{
    
    //szerver felé irányuló socket
    private static Socket clientSocket = null;
    //bemeneti, kimeneti hálózati streamek
    private static BufferedReader is = null;
    private static PrintStream os = null;
    //konzolról olvasó stream
    private static BufferedReader konzolInput = null;

    private static boolean closed = false;


    public static void main(String[] args) {
        
        
        
        try {
            clientSocket = new Socket("localhost", 2222);
            konzolInput = new BufferedReader(new InputStreamReader(System.in));
            os = new PrintStream(clientSocket.getOutputStream());
            is = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException iOException) {
            System.out.println("Nem lehet kapcsolódni a szerverhez");
        }
        
        if (clientSocket != null && os != null && is != null){
            try {
                
                Thread t = new Thread(new H_ChatKliens2());
                t.start();
                while (!closed) {
                    os.println(konzolInput.readLine().trim());
                }
                
                os.close();
                is.close();
                clientSocket.close();
            } catch (IOException iOException) {}
        }
    }

    @Override
    public void run() {
        String responseLine;
        try {
            while ((responseLine = is.readLine()) != null) {
                System.out.println(responseLine);
                if (responseLine.indexOf("*** Bye") != -1){
                    break;
                }
            }
            closed = true;
        } catch (IOException e) {
          System.err.println("IOException:  " + e);
        }    
    }
}
