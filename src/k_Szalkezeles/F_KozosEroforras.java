package k_Szalkezeles;

class FuttathatoRossz implements Runnable {
    
    public static int kozosMemoria = 0;

    private void novel(){
        int segedMemoria = kozosMemoria;
        System.out.println(Thread.currentThread().getName()
                + " - Olvasott érték: " + segedMemoria);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {}
        kozosMemoria = ++segedMemoria;
        System.out.println(Thread.currentThread().getName()
                + " - Kiírt érték: " + segedMemoria);
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            novel();
        }
    }

}

class FuttathatoJo implements Runnable {
    
    public static int kozosMemoria = 0;

    private synchronized void novel(){
        int segedMemoria = kozosMemoria;
        System.out.println(Thread.currentThread().getName()
                + " - Olvasott érték: " + segedMemoria);
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {}
        kozosMemoria = ++segedMemoria;
        System.out.println(Thread.currentThread().getName()
                + " - Kiírt érték: " + segedMemoria);
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            novel();
        }
    }
}

class FuttathatoRossz2 implements Runnable {
    
    public static int kozosMemoria = 0;
    
    private void novel(){
        kozosMemoria++;
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            novel();
        }
    }
}

class FuttathatoJo2 implements Runnable {
    
    public static int kozosMemoria = 0;
    
    private synchronized void novel(){
        kozosMemoria++;
    }
    
    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            novel();
        }
    }
}


public class F_KozosEroforras {
    public static void main(String[] args) {
        //FuttathatoRossz fu = new FuttathatoRossz();
        //FuttathatoJo fu = new FuttathatoJo();
        //FuttathatoRossz2 fu = new FuttathatoRossz2();
        FuttathatoJo2 fu = new FuttathatoJo2();
        Thread t1 = new Thread(fu, "Elso");
        Thread t2 = new Thread(fu, "Masodik");
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException interruptedException) {}
        //join csak azért kell, hogy akkor írjunk ki, ha már minden lefutott
        System.out.println("Szamláló:" + fu.kozosMemoria);
    }
}
