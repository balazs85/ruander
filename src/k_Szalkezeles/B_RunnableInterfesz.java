package k_Szalkezeles;

class RunnableDemo implements Runnable {
    private Thread t;
    private String nev;

    RunnableDemo(String name) {
        nev = name;
        System.out.println("Létrehozva: " + nev);
    }

    @Override
    public void run() {
        System.out.println("Fut: " + nev);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Szál: " + nev + ", számláló: " + i);
                //kis késleltetés a szálnak
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            System.out.println("Szál " + nev + " megszakítva.");
        }
        System.out.println("Szál " + nev + " véget ért.");
    }

    public void start() {
        System.out.println("Elindult: " + nev);
        if (t == null) {
            t = new Thread(this, nev);
            t.start();
        }
    }
}

public class B_RunnableInterfesz {
    public static void main(String args[]) {
        RunnableDemo R1 = new RunnableDemo( "1. szál");
        R1.start();

        RunnableDemo R2 = new RunnableDemo( "2. szál");
        R2.start();
   }  
}
