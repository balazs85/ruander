package k_Szalkezeles;

class ThreadDemo extends Thread {
    private Thread t;
    private String nev;

    ThreadDemo(String name) {
        nev = name;
        System.out.println("Létrehozva: " + nev);
    }

    @Override
    public void run() {
        System.out.println("Fut: " + nev);
        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Szál: " + nev + ", számláló: " + i);
                //kis késleltetés a szálnak
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            System.out.println("Szál " + nev + " megszakítva.");
        }
        System.out.println("Szál " + nev + " véget ért.");
    }

    public void start() {
        System.out.println("Elindult: " + nev);
        if (t == null) {
            t = new Thread(this, nev);
            t.start();
        }
    }

}

public class C_ThreadbolSzarmaztatas {
    public static void main(String args[]) {
        ThreadDemo R1 = new ThreadDemo( "1. szál");
        R1.start();

        ThreadDemo R2 = new ThreadDemo( "2. szál");
        R2.start();
   }  
}
