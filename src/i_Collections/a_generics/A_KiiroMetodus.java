package i_Collections.a_generics;

public class A_KiiroMetodus {
    public static <E> void kiir(E[] tomb )
    {
        for (int i = 0; i < tomb.length; i++) {
            System.out.print(tomb[i]+" ");
        }
        System.out.println();
    }

    public static void main( String args[] )
    {
        Integer[] intArray = { 1, 2, 3, 4, 5 };
        Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
        Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };

        kiir(intArray);
        kiir(doubleArray);
        kiir(charArray); 
    } 
}
