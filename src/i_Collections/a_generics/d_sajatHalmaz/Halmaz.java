package i_Collections.a_generics.d_sajatHalmaz;

import java.lang.reflect.Array;

class Halmaz<T> {

    private final int NOVELES_LEPTEKE = 5;
    private T[] elemek;
    private int elemszam;

    public Halmaz() {
        elemek = (T[]) new Object[0];
        elemszam = 0;
    }

    void hozzaad(T elem) {
        if (elemszam == elemek.length) {
            elemek = java.util.Arrays.copyOf(elemek, elemszam + NOVELES_LEPTEKE);
        }
        elemek[elemszam] = elem;
        elemszam++;
    }

    @Override
    public String toString() {
        String result = "[";
        if (elemszam > 0) {
            for (int i = 0; i < elemszam - 1; i++) {
                result += elemek[i] + ", ";
            }
            result += elemek[elemszam - 1];
        }
        result += "]";
        return result;
    }

    boolean kivesz(T elem) {
        int sorszam = keres(elem);
        if (sorszam == -1){
            return false;
        } else {
            for (int i = sorszam; i < elemszam-1; i++) {
                elemek[i] = elemek[i+1];
            }
            elemszam--;
            return true;
        }
    }

    private int keres(T elem) {
        int i = 0;
        while (i<elemszam && !elemek[i].equals(elem)){
            i++;
        }
        if (i<elemszam){
            return i;
        } else {
            return -1;
        }
    }

    boolean eleme_e(T elem) {
        return keres(elem) != -1;
    }

    int elemszam() {
        return elemszam;
    }
}
