package i_Collections.a_generics.d_sajatHalmaz;

public class Fo {
    public static void main(String[] args) {
        Halmaz<Integer> h = new Halmaz();
        h.hozzaad(10);
        System.out.println(h);
        h.hozzaad(1);
        System.out.println(h);
        h.kivesz(10);
        h.kivesz(10);
        System.out.println(h);
        System.out.println(h.eleme_e(2));
        System.out.println(h.elemszam());
    }
    
}
