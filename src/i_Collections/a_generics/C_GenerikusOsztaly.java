package i_Collections.a_generics;

public class C_GenerikusOsztaly {
    
    private static class Doboz<T> {
        private T t;
        public Doboz() {
        }

        public T get() {
            return t;
        }

        public void set(T t) {
            this.t = t;
        }

    }
    
    public static void main(String[] args) {
        Doboz altalanosDoboz = new Doboz();
        Doboz<String> szovegDoboz = new Doboz();
        Doboz<Integer> egeszDoboz = new Doboz();

        altalanosDoboz.set(3.2);
        szovegDoboz.set("Hello");
        egeszDoboz.set(3);

        System.out.println(altalanosDoboz.get());
        System.out.println(szovegDoboz.get());
        System.out.println(egeszDoboz.get());
        
    }

}
