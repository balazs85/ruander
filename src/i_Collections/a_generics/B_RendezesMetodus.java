package i_Collections.a_generics;

public class B_RendezesMetodus {
   
    public static <E extends Comparable<E>> void rendez(E[] tomb)
    {
        for (int i = 0; i < tomb.length - 1; i++) {
            for (int j = i + 1; j < tomb.length; j++) {
                if (tomb[i].compareTo(tomb[j]) > 0){
                    E c = tomb[i];
                    tomb[i] = tomb[j];
                    tomb[j] = c;
                }
            }
        }
    }
    
    public static <E> void kiir(E[] tomb )
    {
        for (int i = 0; i < tomb.length; i++) {
            System.out.print(tomb[i]+" ");
        }
        System.out.println();
    }
    
    public static void main( String args[] )
    {
        // Create arrays of Integer, Double and Character
        Integer[] intArray = { 1, 4, 333, 2, 5 };
        Double[] doubleArray = { 11.1, 3.3, 2.2, 4.4 };
        Character[] charArray = { 'C', 'B', 'A' };
        
        rendez(intArray);
        rendez(doubleArray);
        rendez(charArray);
        
        kiir(intArray);
        kiir(doubleArray);
        kiir(charArray);
    }     
}
