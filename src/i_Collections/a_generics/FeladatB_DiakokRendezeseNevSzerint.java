package i_Collections.a_generics;

public class FeladatB_DiakokRendezeseNevSzerint {
    static class Diak implements Comparable<Diak>{
        String nev;
        int magassag;

        @Override
        public String toString() {
            return "Diak{" + "nev=" + nev + ", magassag=" + magassag + '}';
        }

        public Diak(String nev, int magassag) {
            this.nev = nev;
            this.magassag = magassag;
        }

        @Override
        public int compareTo(Diak d) {
            return nev.compareTo(d.nev);
        }
    }
    
    public static void main(String[] args) {
        Diak[] osztaly = {
            new Diak("Kovács Béla", 165),
            new Diak("Kiss István", 178),
            new Diak("Antal Olga", 160),
            new Diak("Horvát Helga", 170)
        };
        rendez(osztaly);
        kiir(osztaly);
    }

    public static <E extends Comparable<E>> void rendez(E[] tomb)
    {
        for (int i = 0; i < tomb.length - 1; i++) {
            for (int j = i + 1; j < tomb.length; j++) {
                if (tomb[i].compareTo(tomb[j]) > 0){
                    E c = tomb[i];
                    tomb[i] = tomb[j];
                    tomb[j] = c;
                }
            }
        }
    }
    
    public static <E> void kiir(E[] tomb )
    {
        for (int i = 0; i < tomb.length; i++) {
            System.out.println(tomb[i]);
        }
        System.out.println();
    }
}
