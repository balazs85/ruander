package i_Collections.a_generics;

public class FeladatA_Maximum {
    public static <E extends Comparable<E>> int max(E[] tomb)
    {
        int max = 0;
        for (int i = 1; i < tomb.length; i++) {
            if (tomb[i].compareTo(tomb[max])>0){
                max = i;
            }
        }
        return max;
    }

    public static void main( String args[] )
    {
        // Create arrays of Integer, Double and Character
        Integer[] intArray = { 1, 4, 333, 2, 5 };
        Double[] doubleArray = { 11.1, 3.3, 2.2, 4.4 };
        Character[] charArray = { 'C', 'B', 'A' };
        
        System.out.println(intArray[max(intArray)]);
        System.out.println(doubleArray[max(doubleArray)]);
        System.out.println(charArray[max(charArray)]);
    }      
}
