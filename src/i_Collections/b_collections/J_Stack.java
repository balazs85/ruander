package i_Collections.b_collections;

import java.util.Stack;

public class J_Stack {
    public static void main(String[] args) {
        Stack v = new Stack();
        v.push(1);
        v.push(2);
        v.push(3);
        System.out.println(v.peek());
        System.out.println(v.pop());
        System.out.println(v.pop());
        System.out.println(v.pop());        
    }
}
