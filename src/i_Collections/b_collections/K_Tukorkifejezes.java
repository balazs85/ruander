package i_Collections.b_collections;

import java.util.Stack;

public class K_Tukorkifejezes {

    public static void main(String[] args) {
        String sz = "Géza kék az ég";
        String szMod = sz.replace(" ", "").toLowerCase();
        System.out.println(szMod);
        System.out.println(tukor_e(szMod));
    }

    private static boolean tukor_e(String sz) {
        Stack<Character> v = new Stack();
        int hossz = sz.length();
        int i;

        for (i = 0; i < hossz / 2; i++) {
            v.push(sz.charAt(i));
        }
        
        i = hossz % 2 == 0 ? hossz / 2 : hossz / 2 + 1;

        while(i < hossz && sz.charAt(i)==v.pop()){
            i++;
        }
        
        return i==hossz;
    }
}
