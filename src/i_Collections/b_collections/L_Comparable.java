package i_Collections.b_collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class L_Comparable {

    
    private static class Diak implements Comparable<Diak>{
        private String nev;
        private Integer magassag;

        public Diak() {
            this.nev = "John Doe";
            this.magassag = 0;
        }

        public Diak(String nev, Integer magassag) {
            this.nev = nev;
            this.magassag = magassag;
        }

        @Override
        public int compareTo(Diak o) {
            return magassag.compareTo(o.magassag);
        }

        @Override
        public String toString() {
            return nev + "(" + magassag + ')';
        }
    }
    
    public static void main(String[] args) {
        List<Diak> osztaly = new ArrayList();
        osztaly.add(new Diak("Béla", 178));
        osztaly.add(new Diak("Pisti", 165));
        osztaly.add(new Diak("Martin", 190));
        osztaly.add(new Diak("Anna", 168));
        Collections.sort(osztaly);
        System.out.println(osztaly);
    }

    
}
