package i_Collections.b_collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class C_Halmazmuveletek {

    public static void main(String[] args) {
        Set a = new HashSet();
        a.add(2);
        a.add(4);
        a.add(6);
        a.add(8);
        a.add(10);
        a.add(12);
        a.add(14);

        Set b = new HashSet();
        b.add(3);
        b.add(6);
        b.add(9);
        b.add(12);
        b.add(15);

        Set c = metszet(a, b);
        Set d = unio(a, b);
        Set e = kulonbseg(a, b);
        Set f = kulonbseg(b, a);

        System.out.println("a halmaz elemei: " + a);
        System.out.println("b halmaz elemei: " + b);
        System.out.println("a és b metszete: " + c);
        System.out.println("a és b unioja: " + d);
        System.out.println("a és b különbsége: " + e);
        System.out.println("b és a különbsége: " + f);
    }

    private static Set metszet(Set a, Set b) {
        Set c = new HashSet();
        Iterator it = a.iterator();
        while (it.hasNext()) {
            Object o = it.next();
            if (b.contains(o)) {
                c.add(o);
            }
        }
        return c;
    }

    private static Set unio(Set a, Set b) {
        Set c = new HashSet(a);
        c.addAll(b);
        return c;
    }

    private static Set kulonbseg(Set mibol, Set mit) {
        Set c = new HashSet(mibol);
        c.removeAll(mit);
        return c;
    }
}
