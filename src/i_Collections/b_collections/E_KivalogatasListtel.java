package i_Collections.b_collections;

import java.util.ArrayList;
import java.util.List;

public class E_KivalogatasListtel {
    public static void main(String[] args) {
        List<Integer> al = new ArrayList();
        al.add(2);
        al.add(3);
        al.add(11);
        al.add(20);
        al.add(97);
        al.add(22);
        List<Integer> paros = kivalogat(al);  
        System.out.println(paros);
    }

    private static List<Integer> kivalogat(List<Integer> al) {
        List<Integer> result = new ArrayList();
        for (Integer elem : al) {
            if(elem % 2 == 0){
                result.add(elem);
            }
        }
        return result;
    }
}
