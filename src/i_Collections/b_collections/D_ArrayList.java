package i_Collections.b_collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class D_ArrayList {
    public static void main(String[] args) {
        pelda();
        //pelda2();
    }

    private static void pelda() {
        List al = new ArrayList();
        System.out.println("Kezdeti méret: " + al.size());

        al.add("C");
        al.add("A");
        al.add("E");
        al.add("B");
        al.add("D");
        al.add("F");
        al.add(1, "A2");
        System.out.println("Méret a hozzáadást követően: " + al.size());

        System.out.println("Lista: " + al);
        al.remove("F");
        al.remove(2);
        System.out.println("Méret törlést követően: " + al.size());
        System.out.println("Lista: " + al);
    }

    private static void pelda2() {
        ArrayList al = new ArrayList();
        al.add(2);
        al.add(3);
        al.add(11);
        int osszeg = 0;
        for (Object elem : al) {
            osszeg += (int)elem;
        }
//        for (Iterator iterator = al.iterator(); iterator.hasNext();) {
//            osszeg += (int)iterator.next();
//
//        }

//        for (int i = 0; i < al.size(); i++) {
//            osszeg += (int)al.get(i);
//        }
        
        Iterator iterator = al.iterator();
        while (iterator.hasNext()) {
            osszeg += (int)iterator.next();

        }
        System.out.println(osszeg);
    }
}
