package i_Collections.b_collections;

import java.util.HashMap;
import java.util.Map;

public class G_MaximumMappel {
    public static void main(String[] args) {
        Map<String, Integer> hm = new HashMap();
        hm.put("Kovács Béla", 165);
        hm.put("Kiss István", 178);
        hm.put("Antal Olga", 160);
        hm.put("Horvát Helga", 170);

        int maxMagassag = 0;
        String maxNev = null;
        for (Map.Entry<String, Integer> entrySet : hm.entrySet()) {
            if (entrySet.getValue() > maxMagassag){
                maxNev = entrySet.getKey();   
                maxMagassag = entrySet.getValue();
            }
        }

        System.out.println(maxNev);
    }
}
