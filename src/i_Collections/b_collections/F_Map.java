package i_Collections.b_collections;

import java.util.HashMap;
import java.util.Map;

public class F_Map {

    public static void main(String[] args) {
        pelda1();
        pelda2();

    }

    private static void pelda1() {
        Map hm = new HashMap();
        hm.put("hetfo", 20);
        hm.put("kedd", 18);
        hm.put("szerda", 19);
        hm.put("csutortok", 21);
        hm.put("pentek", 19);

        System.out.println(hm);
    }

    private static void pelda2() {
        Map<String, Integer> hm = new HashMap();
        hm.put("hetfo", 20);
        hm.put("kedd", 18);
        hm.put("szerda", 19);
        hm.put("csutortok", 21);
        hm.put("pentek", 19);

        for (Map.Entry<String, Integer> entrySet : hm.entrySet()) {
            String key = entrySet.getKey();
            Integer value = entrySet.getValue();
            System.out.println(key + " -> " + value);
        }
    }

}
