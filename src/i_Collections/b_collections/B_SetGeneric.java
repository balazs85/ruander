package i_Collections.b_collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class B_SetGeneric {
    public static void main(String[] args) {
        Set<Integer> hs = new HashSet();
        hs.add(2);
        hs.add(11);
        hs.add(1);
        System.out.println(hs);
        hs.remove(2);
        System.out.println(hs);
    }    
}
