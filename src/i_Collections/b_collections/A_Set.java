package i_Collections.b_collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class A_Set {
    public static void main(String[] args) {
        HashSet hs = new HashSet();
        hs.add("B");
        hs.add("A");
        hs.add("D");
        hs.add("E");
        hs.add("C");
        hs.add("F");
        System.out.println(hs);
        
        for (Iterator iterator = hs.iterator(); iterator.hasNext();) {
            System.out.print(iterator.next()+" "); 
        }
        System.out.println("");

        Iterator it = hs.iterator();
        while(it.hasNext()){
            System.out.print(it.next()+" "); 
        } 
    }    
}
