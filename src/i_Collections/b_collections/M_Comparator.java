package i_Collections.b_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class M_Comparator {
    private static class Diak{
        private String nev;
        private Integer magassag;

        public Diak() {
            this.nev = "John Doe";
            this.magassag = 0;
        }

        public Diak(String nev, Integer magassag) {
            this.nev = nev;
            this.magassag = magassag;
        }

        @Override
        public String toString() {
            return nev + "(" + magassag + ')';
        }
    }

    
    public static void main(String[] args) {
        List<Diak> osztaly = new ArrayList();
        osztaly.add(new Diak("Béla", 178));
        osztaly.add(new Diak("Pisti", 165));
        osztaly.add(new Diak("Martin", 190));
        osztaly.add(new Diak("Anna", 168));
        
        //Collections.sort(osztaly, new nevComparator());
        osztaly.sort(new nevComparator());
        System.out.println(osztaly);
        osztaly.sort(new magassagComparator());
        System.out.println(osztaly);
    }

        private static class nevComparator implements Comparator<Diak>{

            @Override
            public int compare(Diak o1, Diak o2) {
                return o1.nev.compareTo(o2.nev);
            }
        }
    
        private static class magassagComparator implements Comparator<Diak>{

        @Override
        public int compare(Diak o1, Diak o2) {
            return o1.magassag.compareTo(o2.magassag);
        }
    }
}
