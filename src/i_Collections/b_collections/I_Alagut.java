package i_Collections.b_collections;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class I_Alagut {
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
        Queue<Date> s = new LinkedList();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        double uthosszKmBen = 0.15;
        double sebessegkorlat = 70;
        BufferedReader br = new BufferedReader(new FileReader("alagut.txt"));
        String sor;
        int atleptekDB = 0;
                
        while ((sor = br.readLine()) != null) {            
            String[] darabok = sor.split(" ");
            Date t = sdf.parse(darabok[1]);
            if (darabok[0].equals("B")){
                s.add(t);
            } else {
                Date sorbol = s.remove();
                double kulonbsegHBan = (double)(t.getTime()-sorbol.getTime())/(1000*60*60);
                double atlagsebesseg = uthosszKmBen/kulonbsegHBan;
                System.out.println(atlagsebesseg);
                if (atlagsebesseg > sebessegkorlat){
                    atleptekDB++;
                }
            }
        }
        System.out.println("Legalább "+atleptekDB+" kocsi lépte át a korlátozást.");
        br.close();
    }
}
