package b_VezerlesiSzerkezetek.b_Ciklus;

import java.util.Scanner;

public class CiklusB {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int egesz;
        do {
            System.out.println("Adjon meg egy pozitív egészet!");
            egesz = in.nextInt();
        } while (egesz <= 0);
        System.out.println(egesz);
    }

}
