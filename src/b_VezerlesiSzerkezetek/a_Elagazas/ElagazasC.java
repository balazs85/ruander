package b_VezerlesiSzerkezetek.a_Elagazas;

public class ElagazasC {
    public static void main(String[] args){
        int honap = 8;
        String honapNev;
        switch (honap) {
            case 1:  honapNev = "Január";
                     break;
            case 2:  honapNev = "Február";
                     break;
            case 3:  honapNev = "Március";
                     break;
            case 4:  honapNev = "Április";
                     break;
            case 5:  honapNev = "Május";
                     break;
            case 6:  honapNev = "Június";
                     break;
            case 7:  honapNev = "Július";
                     break;
            case 8:  honapNev = "Augusztus";
                     break;
            case 9:  honapNev = "Szeptember";
                     break;
            case 10: honapNev = "Október";
                     break;
            case 11: honapNev = "November";
                     break;
            case 12: honapNev = "December";
                     break;
            default: honapNev = "Nincs ilyen hónap";
                     break;
        }
        System.out.println(honapNev);
    }

}
