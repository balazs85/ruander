package b_VezerlesiSzerkezetek.a_Elagazas;

public class ElagazasB {
    public static void main(String[] args){
        int pontszam = 76;
        char jegy;

        if (pontszam >= 90) {
            jegy = '5';
        } else if (pontszam >= 80) {
            jegy = '4';
        } else if (pontszam >= 70) {
            jegy = '3';
        } else if (pontszam >= 60) {
            jegy = '2';
        } else {
            jegy = '1';
        }
        System.out.println("Jegyed: " + jegy);
    }

}
