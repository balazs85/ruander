package b_VezerlesiSzerkezetek.a_Elagazas;

public class ElagazasD {
    public static void main(String[] args){
        int honap = 12;
        String evszak;
        switch (honap) {
            case 1: case 2: case 12:
                evszak = "Tél";
                break;
            case 3: case 4: case 5:
                evszak = "Tavasz";
                break;             
            case 6: case 7: case 8:
                evszak = "Nyár";
                break;    
            case 9: case 10: case 11:
                evszak = "Ősz";
                break;    
            default: evszak = "Nincs ilyen évszak";
                     break;
        }
        System.out.println(evszak);
    }

}
